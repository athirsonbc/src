// @author  Diogo_RBG - http://diogorbg.blogspot.com
// @review  Jesusalva - http://manabird.servegame.com/
// @desc    Memorial em homenagem aos vencedores dos eventos de páscoa.


005-1,44,73,0|script|Memorial de Páscoa|344
{
    mes "[Memorial Páscoa 2011]";
    mes "O reino de Bhramir saúda os grandes guerreiros Dragon, Super Sasuke, lle0, alasmirt, Knight, Katucha, RecoN, Rosa de Sar om, ryanzinho, pepe, MilwalKe, 220Volts, Gladiador Flash, Dario Conca e -Kim- por seus atos grandiosos.";
    mes "";
    mes "Este memorial é dedicado a todos eles... toda a glória a eles!!!";
    next;
    mes "[Memorial Páscoa 2011]";
    mes "1º - 1404 - Dragon (99)";
    mes "2º - 1260 - Super Sasuke (98)";
    mes "3º - 1247 - lle0 (96)";
    mes "4º - 1188 - alasmirt (77)";
    mes "5º - 1068 - Knight (99)";
    mes "6º - 1015 - Katucha (92)";
    mes "7º - 926 - RecoN (77)";
    mes "8º - 905 - Rosa de Sar om (90)";
    mes "9º - 815 - ryanzinho (82)";
    mes "10º - 792 - pepe (84)";
    mes "11º - 761 - MilwalKe (97)";
    mes "12º - 719 - 220Volts (85)";
    mes "13º - 704 - Gladiador Flash (89)";
    mes "14º - 559 - Dario Conca (91)";
    mes "15º - 530 - -Kim- (90)";
    close2;
    mes "[Memorial Páscoa 2017]";
    mes "O reino de Bhramir saúda os bravos guerreiros DarKSlderS, Sky Dragon! e Jesusalva por seus atos dignos de nota.";
    mes "";
    mes "Este memorial é dedicado a todos eles.";
    next;
    mes "[Memorial Páscoa 2017]";
    mes "1º - DarKSlderS";
    mes "2º - Sky Dragon!";
    mes "3º - Jesusalva";
    close;
}

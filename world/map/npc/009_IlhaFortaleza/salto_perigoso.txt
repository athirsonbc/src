009,88,84,0|script|#pularPonteMorte1|332
{
    if (QUEST_pularPonte != 0) goto L_pular;
    if (isin("009", 86, 76, 87, 76)) goto L_indeciso;
    message strcharinfo(0), "Não é possível pular dessa distância.";
    end;

L_indeciso:
    mes "[" + strcharinfo(0) + "]";
    mes "\"Hmm... não sei se pular dessa ponte é uma boa ideia.\"";
    close;

L_pular:
    mes "Você olha para baixo e então se decide.";
    mes "";
    menu
        "Pular",  L_pulou,
        "Recuar", L_Fechar;

L_pulou:
    warp "009", 0, 0;
    mes "";
    mes "Se deu mal! Pulou em águas rasas.";
    heal -100, 0;
    message strcharinfo(0), "Ai!";
    close;

L_Fechar:
    close;
}


009,87,84,0|script|#pularPonteMorte2|332,0,0
{
    if (QUEST_pularPonte != 0) goto L_pular;
    if (isin("009", 86, 76, 87, 76)) goto L_indeciso;
    message strcharinfo(0), "Não é possível pular dessa distância.";
    end;

L_indeciso:
    mes "[" + strcharinfo(0) + "]";
    mes "\"Hmm... não sei se pular dessa ponte é uma boa ideia.\"";
    close;

L_pular:
    mes "Você olha para baixo e então se decide.";
    mes "";
    menu
        "Pular",  L_pulou,
        "Recuar", L_Fechar;

L_pulou:
    warp "009", 0, 0;
    mes "";
    mes "Se deu mal! Pulou em águas rasas.";
    heal -100, 0;
    message strcharinfo(0), "Ai!";
    close;

L_Fechar:
    close;
}


009,86,84,0|script|#pularPonteMorte3|332
{
    if (QUEST_pularPonte != 0) goto L_pular;
    if (isin("009", 86, 76, 87, 76)) goto L_indeciso;
    message strcharinfo(0), "Não é possível pular dessa distância.";
    end;

L_indeciso:
    mes "[" + strcharinfo(0) + "]";
    mes "\"Hmm... não sei se pular dessa ponte é uma boa ideia.\"";
    close;

L_pular:
    mes "Você olha para baixo e então se decide.";
    mes "";
    menu
        "Pular",  L_pulou,
        "Recuar", L_Fechar;

L_pulou:
    warp "009", 0, 0;
    mes "";
    mes "Se deu mal! Pulou em águas rasas.";
    heal -100, 0;
    message strcharinfo(0), "Ai!";
    close;

L_Fechar:
    close;
}


009,88,85,0|script|#pularPonteOk|332
{
    if (QUEST_pularPonte != 0) goto L_pular;
    if (!(isin("009", 86, 76, 87, 76))) message strcharinfo(0), "Não é possível pular dessa distância.";
    mes "\"Não sei se pular dessa ponte é uma boa ideia.\"";
    close;

L_pular:
    mes "Você olha para baixo e então se decide.";
    mes "";
    menu
        "Pular",  L_pulou,
        "Recuar", L_Fechar;

L_pulou:
    warp "009", 88, 85;
    set QUEST_pularPonte, 2;
    heal -90, 0;
    message strcharinfo(0), "Ai!";
//  if (hp == 0) goto L_quase;
    mes "";
    mes "Era realmente muito alto, mas você pulou em águas profundas.";
    close;

//L_quase:
//    mes "";
//    mes "Você teria conseguido se já não estivesse ferido.";
//    close;

L_Fechar:
    close;
}


009,89,81,0|script|Afogado|300,2,5
{
    if (isin("009", 86, 76, 87, 76)) goto L_naoPule;
    if (QUEST_pularPonte == 2 && (isin("009", 88, 85, 88, 85))) goto L_nadarPraia;

    mes "[Afogado]";
    mes "\"Não posso te ouvir direito dessa distância.\"";
    close;

L_naoPule:
    mes "[Afogado]";
    mes "\"Não pule dessa ponte... pode ser fatal.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Mas eu preciso chegar do outro lado... talvez você possa me ajudar.", L_ajuda,
        "Isso nem passava pela minha cabeça.", L_Tchau;

L_Tchau:
    mes "";
    mes "[Afogado]";
    mes "\"Que bom.\"";
    close;

L_ajuda:
    set QUEST_pularPonte, 1;
    mes "";
    mes "[Afogado]";
    mes "\"Talvez eu possa.\"";
    next;
    mes "[Afogado]";
    mes "\"O que eu sei é que aqui onde eu pulei não é um bom lugar... e se alguém tivesse me avisado disso eu não estaria aqui.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "\"Alí parece ser um bom lugar... tomara que dê certo.\"";
    next;
    mes "[Afogado]";
    mes "\"Tomara é que sua armadura lhe projeta.\"";
    close;

L_nadarPraia:
    mes "[Afogado]";
    mes "\"É, parece que você conseguiu mesmo !\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "\"Obrigado pela dica. Até mais.\"";
    next;
    warp "009", 88, 91;
    set QUEST_pularPonte, 0;
    mes "[Afogado]";
    mes "\"Sujeito de sorte... é isso que você é !\"";
    close;

OnTouch:
    message strcharinfo(0), "... NÃO FAÇA ISSO !!!";
    end;
}

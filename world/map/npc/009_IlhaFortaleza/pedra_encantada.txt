// Ponte (1)
009,81,76,0|script|Pedra Encantada|400
{
    if (!(isin("009", 80, 75, 82, 77))) goto L_muitoLonge;

    if (QUEST_pedraEncantada == 3) goto L_teleporte;

    mes "[Pedra Encantada]";
    mes "\"Ao tocar na Pedra Encantada você sente uma enorme energia fluir através de todo o seu corpo.\"";
    next;
    if (QUEST_pedraEncantada == 2) goto L_primeiroTeleporte;
    set QUEST_pedraEncantada, 1;
    mes "[Pedra Encantada]";
    mes "\"Mas essa energia vai perdendo força e nada acontece.\"";
    close;

L_muitoLonge:
    mes "[" + strcharinfo(0) + "]";
    mes "\"Muito longe para tocar na pedra.\"";
    close;

L_primeiroTeleporte:
    set QUEST_pedraEncantada, 3;
    mes "[Pedra Encantada]";
    mes "\"Então você se lembra de ter sentido essa mesma energia ao tocar em outra pedra.\"";
    next;
    warp "009-3", 166, 53;
    mes "[Pedra Encantada]";
    mes "\"Dê repente você se sente sugado por essa energia... e quando retoma sua consciência está junto da outra pedra.";
    mes "É como se a pedra pudesse te levar ao encontro de seu pensamento.\"";
    close;

L_teleporte:
    mes "[Pedra Encantada]";
    mes "\"Ao tocar na Pedra Encantada e sentir a energia fluir você mentaliza a Pedra Encantada do Lar das Fadas.\"";
    next;
    warp "009-3", 166, 53;
    mes "[Pedra Encantada]";
    mes "\"A energia toma conta de seu corpo e em instantes você é levado até lá.\"";
    close;
}

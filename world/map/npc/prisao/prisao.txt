// @autor Diogo_RBG


//prisao,20,24,0|script|debug|307
//{
//    mes "Você nunca foi preso!";
//    set prisEst, 0;
//    close;
//}


prisao,22,22,0|script|Delegado Jenésio|150
{
    callfunc "GameRules";
    close;
}

prisao,38,25,0|script|Guarda Clóvis|307
{
    if (prisEst == 1) goto L_foiPreso;
    if (prisEst == 2) goto L_aguarde;
    if (prisEst == 3) goto L_deOlho;

    mes "[Guarda Clóvis]";
    mes "\"Você sim é um cidadão exemplar! Nunca foi preso!";
    mes "Ao contrário destes sujeitos de ficha suja.\"";
    npctalk strnpcinfo(0), "'" + strcharinfo(0) + "' é um cidadão exemplar! Possui a ficha limpa!";
    close;

L_foiPreso:
    mes "[Guarda Clóvis]";
    mes "\"Outro que não respeita ou não conhece as Leis deste Mundo!\"";
    next;
    mes "[Guarda Clóvis]";
    mes "\"Fale como Delegado que ele te dirá quais são estas Leis!\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "=S Como vim parar aqui?", L_Porque,
        "=o Sou inocente... me solte!", L_inocente;

L_Porque:
    mes "[Guarda Clóvis]";
    mes "\"É uma boa pergunta!\"";
    next;
    goto L_explicacao;

L_inocente:
    mes "[Guarda Clóvis]";
    mes "\"Isso é o que todos dizem!\"";
    next;
    goto L_explicacao;

L_explicacao:
    mes "[Guarda Clóvis]";
    mes "\"Você foi preso porque descumpriu umas das leis do reino.";
    mes "Por isso terá que pagar pena na prisão!\"";
    set $prisTime[prisNum], 0;
    set prisEst, 2;
    close;

L_aguarde:
    if ($prisTime[prisNum] >= prisPena) goto L_cumpriuPena;

    mes "[Guarda Clóvis]";
    set @n, prisPena - $prisTime[prisNum];
    if (@n == 1) mes "\"Sua pena está quase terminando. Será libertado em 1 minuto!\"";
    if (@n > 1)  mes "\"Sua pena ainda não terminou. Ainda faltam " + @n + " minutos.\"";
    next;
    mes "[Guarda Clóvis]";
    mes "\"Enquanto isso informe-se sobre as Leis deste Mundo com o Delegado!\"";
    close;

L_cumpriuPena:
    mes "[Guarda Clóvis]";
    mes "\"Você cumpriu toda a pena!";
    mes "Mas lembre-se que se tornar a cometer crimes contra o reino será enviado para prisão outra vez!\"";
    next;
    mes "[Guarda Clóvis]";
    mes "\"Para não ser preso novamente informe-se sobre as Leis deste Mundo com o Delegado!\"";
    next;
    mes "[Guarda Clóvis]";
    mes "\"Agora você está livre!";
    mes "Faça bom uso de sua liberdade.\"";
    set @x, rand(11);
    warp "prisao", 25+@x, 27;
    //savepoint "prisao", 25+@x, 27; // Salvar na prisao é errado... calsa brecha.
    savepoint "005", 132, 82;
    callfunc "delPris";
    close;

L_deOlho:
    mes "[Guarda Clóvis]";
    mes "\"Agora você está fichado. Melhor não cometer mais nenhum crime!\"";
    if (prisQtd == 1) npctalk strnpcinfo(0), "'" + strcharinfo(0) + "' foi fichado uma vez!";
    if (prisQtd > 1) npctalk strnpcinfo(0), "'" + strcharinfo(0) + "' foi fichado " + prisQtd + " vezes!";
    close;

//- Mostrando emotion -------------------------------------------------

//OnTimer60000:
//    emotion 14;
//    setnpctimer 0;
//    end;
//
//OnInit:
//    initnpctimer;
//    end;
//}

//= Vigiando prisioneiros =============================================

//prisao,0,0,0|script|#timerPrisao|300
//{
//    end;
//OnTimer60000:
//    set $@i, 0;
//    L_loop: set $@i, $@i + 1; if ($@i > $numPris) goto L_fimLoop;
//        if ($prisTime[$@i] == -1 || isloggedin($prisPlay[$@i]) == 0) goto L_loop;
//        attachrid ($prisPlay[$@i]);
//        if (prisNum != $@i) goto L_loop; // Está logado com outro char!
//        set $prisTime[$@i], $prisTime[$@i] + 1;
//        goto L_loop;
//    L_fimLoop:
//    setnpctimer 0;
//end;
//
//OnInit:
//    initnpctimer;
//    end;
//
//}

//= Funções ===========================================================

function|script|addPris
{
    if (prisEst != 0) goto L_jaFoiPreso;

    set $numPris, $numPris + 1;
    set prisNum, $numPris;
    set $prisPlay[prisNum], getcharid(3);
    set $prisTime[prisNum], -1;
    set prisPena, 15;
    //resetlvl 4;        // Tirava os pontos de status
    //set SkillPoint, 0; // Zerava a skill Basic
    setskill SKILL_TRADE, 0; // Reseta a skill de trade.
    set prisQtd, 1;
    set prisEst, 1;
    return;

L_jaFoiPreso:
    set $prisTime[prisNum], -1;
    set prisPena, 15;
    set prisQtd, prisQtd + 1;
    //resetlvl 4;        // Tirava os pontos de status
    //set SkillPoint, 0; // Zerava a skill Basic
    setskill SKILL_TRADE, 0; // Reseta a skill de trade.
    set prisEst, 1;
    return;
}

function|script|delPris
{
    if (prisEst == 0 || prisEst == 3) goto L_Return;

    if (prisQtd > 0) set prisQtd, prisQtd - 1;
    set $prisTime[prisNum], -1;
    set prisEst, 3;
    return;

L_Return:
    return;
}

hali001,44,21,0|script|Vadre|512,1,1
{
    if ($PosseDeHalicarnazo != 0 && Reino != $PosseDeHalicarnazo) goto L_SugestaoAoInvasor;

    mes "[Vadre]";
    mes "Cuidado, pois o que tenho para te oferecer serve somente para aqueles que buscam a força suprema.";
    next;
    mes "[Vadre]";
    mes "Deseja que eu te mostre?";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Sim!", L_comprar,
        "Não!", L_Fechar;

L_SugestaoAoInvasor:
    heal -(MaxHp / 10), 0;
    set @n, rand(4);
    if (@n == 0) npctalk strnpcinfo(0), "Saia daqui seu invasor de Halicarnazo!";
    if (@n == 1) npctalk strnpcinfo(0), "Você não pode entrar em Halicarnazo!";
    if (@n == 2) npctalk strnpcinfo(0), "Como você se atreve a tentar machucar as pessoas do Reino de Halicarnazo?";
    if (@n == 3) npctalk strnpcinfo(0), "Eu te mostrarei do que o povo de Halicarnazo é feito.";
    end;

L_comprar:
    mes "";
    mes "[Vadre]";
    mes "Veja o poder supremo!";
    close2;
    shop "#Vadre";

L_Fechar:
    close;
}

hali001,44,21,0|shop|#Vadre|32767,BastaoBranco:100000,BastaoAmarelo:200000,BastaoVerde:400000,BastaoAzul:800000,BastaoVermelho:1600000,BastaoRoxo:3200000,BastaoNegro:6400000

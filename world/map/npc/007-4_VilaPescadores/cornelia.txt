// @script Cornélia
// @autor Diogo_RBG - http://diogorbg.blogspot.com
// @desc Uma bondosa e atenciosa dona de casa que irá lhe ajudar de bom grado.
// #quest <desc>, <getItens>, <delItens>, <extra>
// @quest 'Tortas de Maçã', '8 tortaMaca', '1 cestaMacas', 'Esperar 5:00'
// @quest 'Rolo de Macarrão', '1 roloMacarrao, 180 exp', '', 'Matar 15 Vermes'
//

//007-4,18,27,0|script|debug|140
//{
//    menu
//        "0 - zerar", -,
//        "1 - procurar", -,
//        "2 - esperar maçãs", -,
//        "3 - levar cesta", -,
//        "4 - esperar torta", -,
//        "5 - levar torta", -,
//        "6 - fim", -;
//    set QUEST_praia, @menu-1;
//    if (@menu == 1) set QUEST_contamobs, 0;
//    callfunc "getTime";
//    set QUEST_praiaTime, @time;
//    mes "resetado para "+(@menu-1);
//    close;
//}


007-4,21,27,0|script|Cornélia|140,10,10
{
    if (QUEST_praia == 1) goto L_encontre;
    if (QUEST_praia == 2) goto L_colhendo;
    if (QUEST_praia == 3) goto L_entregarCesta;
    if (QUEST_praia == 4 && gettimetick(2) >= QUEST_praiaTime + 5 * 60) goto L_tortaPronta;
    if (QUEST_praia == 4) goto L_tortaAssando;
    if (QUEST_praia == 5 || QUEST_praia == 6) goto L_obrigada;
    if (QUEST_praia == 7) goto L_contagemVermes;
    if (QUEST_praia >= 8 && QUEST_praia <= 12) goto L_esperapresente;
    if (QUEST_praia >= 13) goto L_ganhoupresente;
    goto L_Inicio;

L_Inicio:
    mes "[Cornélia]";
    mes "\"Olá " + @my$ + " jovem. Você poderia me ajudar?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Mas é claro que sim.", L_sim,
        "Outra hora.",          L_Fechar;

L_sim:
    mes "";
    mes "[Cornélia]";
    mes "\"Estou muito preocupada com minha prima Joana. Ela saiu para colher umas maçãs e ainda não voltou.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Porque ela foi colher maçãs?", L_Porque,
        "Ela deve voltar logo.",        L_Fechar;

L_Porque:
    mes "";
    mes "[Cornélia]";
    mes "\"É que eu precisava de algumas maças para fazer uma deliciosa torta de maças e eu não tinha nenhuma!\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "E onde você acha que ela foi colher maçãs?", L_onde,
        "Ela deve voltar logo.",                      L_Fechar;

L_onde:
    mes "";
    mes "[Cornélia]";
    mes "\"Ela costumava colher maçãs próxima a casa de Jack o lenhador, pois era lá que costumávamos catar maçãs quando éramos crianças.";
    mes "Mas acredito que ela deva estar colhendo maçãs em seu próprio quintal.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "E onde fica a casa dela?", L_OndeFica;

L_OndeFica:
    mes "";
    mes "[Cornélia]";
    mes "\"Ela mora em uma casa à beira mar na praia ao norte.";
    mes "Ela possui um lindo quital gramado e coberto de flores... não será difícil encontrá-la!\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Não se preocupe. Irei encontrá-la e dizer que você está preocupada com ela.", L_Encontrar;

L_Encontrar:
    mes "";
    mes "[Cornélia]";
    mes "\"Muito obrigada!\"";
    set QUEST_praia, 1;
    close;

L_encontre:
    mes "[Cornélia]";
    mes "\"Por favor encontre minha prima Joana. Ela deve estar colhendo maçãs no quintal de sua casa.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Não se preocupe. Irei encontrá-la.", L_Encontrar2;

L_Encontrar2:
    mes "";
    mes "[Cornélia]";
    mes "\"Por favor não demore!\"";
    close;

//=====================================================================

L_colhendo:
    mes "[Cornélia]";
    mes "\"Encontrou minha prima?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Sim... ela está colhendo maçãs no quintal de sua casa.", L_Encontrei;

L_Encontrei:
    mes "[Cornélia]";
    mes "\"Estou aliviada... muito obrigada!\"";
    goto L_questVermes;

//=====================================================================

L_entregarCesta:
    mes "[Cornélia]";
    mes "\"Encontrou minha prima?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Sim... e aqui estão as maçãs que ela colheu para você.", L_Pronto;

L_Pronto:
    mes "";
    mes "[Cornélia]";
    mes "\"Estou aliviada... muito obrigada!\"";
    next;
    mes "[Cornélia]";
    mes "\"Gostaria de provar a minha deleciosa toda de maçã?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "\"Sim. Deve ser deliciosa mesmo, pois todos falam dela.\"";
    next;
    if (countitem("CestaMacas") < 1) goto L_semCesta;
    mes "[Cornélia]";
    mes "\"Irei prepará-la agora mesmo!";
    mes "Só vai levar 5 minutos.\"";
    delitem "CestaMacas", 1;
    set QUEST_praiaTime, gettimetick(2);
    set QUEST_praia, 4;
    close;

L_semCesta:
    mes "[Cornélia]";
    mes "\"Mas primeiro eu preciso que você me traga a cesta de maçãs que minha prima mandou para mim.";
    mes "Você disse que estava com a cesta, mas ela não está aí.\"";
    close;

L_tortaAssando:
    mes "[Cornélia]";
    mes "\"A torta ainda está assando... por favor espere alguns minutos.\"";
    close;

L_tortaPronta:
    getinventorylist;
    if ((@inventorylist_count + (countitem("TortaDeMaca") == 0)) > 100) goto L_cheio;
    set QUEST_praiaTime, 0;
    mes "[Cornélia]";
    mes "\"A torta de maçã está pronta!";
    mes "Fique com estas 15 fatias. A outra vou guardar para minha prima.\"";
    getitem "TortaDeMaca", 15;
    set QUEST_praia, 5;
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "\"Sua prima avisou que não viria hoje.\"";
    next;
    mes "[Cornélia]";
    mes "\"Neste caso vou precisar novamente da sua ajuda. Leve esta outra fatia para ela, por favor.\"";
    getitem "TortaDeMaca", 1;
    close;

L_cheio:
    mes "";
    mes "[Cornélia]";
    mes "\"Pretendia lhe dar algo, mas parece que seu inventário está cheio.";
    mes "Volte mais tarde.\"";
    close;

L_obrigada:
    mes "[Cornélia]";
    mes "\"Obrigada por me ajudar com a torta. Eu agora só espero que o Pablo goste... Pois este é o meu presente de aniversário de casamento para ele...\"";
    next;
    mes "[Cornélia]";
    mes "\"Espero que este ano ele capriche no meu presente, pois ano passado fiquei com a impressão de que ele só se lembrou da data em cima da hora...\"";
    goto L_questVermes;

L_esperapresente:
    mes "[Cornélia]";
    mes "\"Não vejo a hora de ver o presente que o Pablo comprou para mim!\"";
    close;

L_ganhoupresente:
    mes "[Cornélia]";
    mes "\"Nossa... o meu marido Pablo é tão romântico e dedicado... o presente dele foi uma rosa branca, um simbolo de amor eterno. \"";
    close;

//=====================================================================

L_questVermes:
    if (QUEST_contamobs == 0) goto L_roloMacarrao;
    if (QUEST_contamobs >= 2) goto L_contagemVermes;
    close;

L_roloMacarrao:
    next;
    mes "[Cornélia]";
    mes "\"Meu marido Pablo me disse que você é guerreiro! É verdade?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Sim. Eu luto contra todo tipo de monstro.", L_OK1,
        "Não quero falar sobre isso.",               L_Fechar;

L_OK1:
    mes "";
    mes "[Cornélia]";
    mes "\"Nossa! Você realmente é um guerreiro destemido!";
    mes "Eu gostaria muito de te ajudar em sua jornada, mas não tenho muito a oferecer.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Eu não recusaria ajuda dada de bom grado.", L_OK2;

L_OK2:
    getinventorylist;
    if ((@inventorylist_count + (countitem("RoloDeMacarrao") == 0)) > 100)
        goto L_cheio;
    mes "";
    mes "[Cornélia]";
    mes "\"Neste caso fique com este Rolo de Macarrão. Não é exatamente uma arma, mas servirá para matar alguns Vermes.\"";
    getitem "RoloDeMacarrao", 1;
    set QUEST_praia, 7;
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Matar Vermes?", L_Vermes;

L_Vermes:
    mes "";
    mes "[Cornélia]";
    mes "\"Sim. Isto servirá para ajudar em sua evolução.";
    mes "Mate 15 Vermes com o Rolo de Macarrão que te dei e verá como você se tornará mais forte.";
    close;

L_contagemVermes:
    next;
    mes "[Cornélia]";
    mes "\"E então! Já matou os vermes que te falei?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Já matei " + (QUEST_contamobs) + " Vermes.", L_ContaMob;

L_ContaMob:
    if (QUEST_contamobs >= 15) goto L_completou;
    mes "";
    mes "[Cornélia]";
    mes "\"Essa quantia é insuficiente. Você precisa matar pelo menos 15 Vermes se quiser se tornar mais forte.\"";
    close;

L_completou:
    mes "";
    mes "[Cornélia]";
    mes "\"Parabéns... você matou uma boa quantia de Vermes e deve ter ganhado uma boa experiência por isso.";
    mes "";
    mes "* Você ganhou 180 pontos de experiência.";
    set BaseExp, BaseExp + 180;
    set QUEST_contamobs, 0;
    set QUEST_praia, 8;
    close;

L_Fechar:
    close;

OnTouch:
    if (QUEST_praia < 8) emotion EMOTE_QUEST;
    end;

//OnTimer10000:
//    set @n, rand(3);
//    if (@n == 0) npctalk strnpcinfo(0), "La! La! Ri! La! ♪";
//    if (@n == 1) npctalk strnpcinfo(0), "La! La! La! La! Ri! Ri! Ra! ♪";
//    if (@n == 2) npctalk strnpcinfo(0), "Ri! Ri! Ri! Ra! ♪";
//    setnpctimer 0;
//    end;
//
//OnInit:
//    initnpctimer;
//    end;
}

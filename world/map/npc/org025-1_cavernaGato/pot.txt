// O pote no qual o jogador pode depositar itens para o gato
// Traduzido e adaptado por: Sky!


org025-1,37,29,0|script|pote|127
{
    if (Katze > 0)
        mes "É o mesmo pote velho.";
    if (Katze == 0)
        mes "É um pote.";
    mes "";

    if (Katze == 1 && @katzeBeenOutside == 1)
        goto L_NeedsFood;
    if (Katze == 1)
        goto L_HasMilk;
    if (Katze == 2 && @katzeBeenOutside == 1)
        goto L_NeedsFur;
    if (Katze == 2)
        goto L_HasFood;
    if (Katze == 3)
        goto L_NeedsWood;
    if (Katze == 4 && @katzeBeenOutside == 0)
        goto L_HasWood;
    if (Katze >= 4)
        goto L_End;

    if (countitem("Leite") > 0)
        menu
            "Colocar um pouco de leite.", L_GiveMilk,
            "Deixar pra lá.",             L_End;
    close;

L_GiveMilk:
    delitem "Leite", 1;
    set Katze, 1;
    set @katzeBeenOutside, 0;
    close;

L_HasMilk:
    mes "Tem leite dentro do pote.";
    close;

L_NeedsFood:
    mes "O leite sumiu!";
    mes "";
    if (countitem("CoxaDeFrango") > 0 && countitem("File") > 0)
        menu
            "Colocar uma Coxa de Frango dentro.", L_GiveChicken,
            "Colocar um Filé dentro.",            L_GiveSteak,
            "Deixar pra lá.",                     L_End;

    if (countitem("CoxaDeFrango") > 0 && countitem("File") == 0)
        menu
            "Colocar uma Coxa de Frango dentro", L_GiveChicken,
            "Deixar pra lá", L_End;

    if (countitem("CoxaDeFrango") == 0 && countitem("File") > 0)
        menu
            "Colocar um Filé dentro.", L_GiveSteak,
            "Deixar pra lá.",          L_End;
    close;

L_GiveChicken:
    delitem "CoxaDeFrango", 1;
    set Katze, 2;
    set @katzeBeenOutside, 0;
    close;

L_GiveSteak:
    delitem "File", 1;
    set Katze, 2;
    set @katzeBeenOutside, 0;
    close;

L_HasFood:
    mes "Há um pouco de comida dentro.";
    close;

L_NeedsFur:
    mes "E está vazio!";
    next;
    if (countitem("PeloBranco") > 0)
        menu
            "Por um Pelo Branco perto do pote.", L_GiveFur,
            "Deixar pra lá.",                    L_End;
    close;

L_GiveFur:
    mes "";
    mes "Você coloca o pelo perto do pote, mas a gata não parece reagir. Talvez há algo mais que você possa fazer. Você pega o pelo de volta.";
    close;

L_NeedsWood:
    if (countitem("TocoDeMadeira") > 0)
        menu
            "Por um Toco de Madeira perto do pote.", L_GiveWood,
            "Deixar pra lá.",                        L_End;
    close;

L_GiveWood:
    delitem "TocoDeMadeira", 1;
    set Katze, 4;
    set @katzeBeenOutside, 0;
    mes "";
    mes "Você coloca o toco perto do pote. A gata olha com atenção, mas continua em seu lugar.";
    close;

L_HasWood:
    mes "Um toco de madeira está ao lado do pote.";
    close;

L_End:
    close;
}

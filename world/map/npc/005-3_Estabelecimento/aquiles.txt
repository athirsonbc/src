// Quest do escudo negro feita por Xtreem para um item feito muito tempo atrás por 14k FALL.
// Como Luno ja falou o que esta no forum nos pertence então vou criar a quest para este item que esta esquecido.
// Créditos ao meu irmão Gabriel (Loricasso), por fazer as correções ortográficas.
// http://forums.themanaworld.com.br/viewtopic.php?f=57&t=5568&p=39023#p38844

005-3,30,52,0|script|Aquiles|511
{
    set @EscudoNegro, 4051; // <= Não existe o item 4051
    set @Bolota, 743;
    set @CasuloDeSeda, 718;
    set @CristalEscuro, 631;
    set @PataDeInseto, 518;
    set @GosmaDeVerme, 505;
    set @EscamasDeDragao, 3013;
    set @ChaveDoTesouro, 537;
    set @Balinha, 510;
    set @BolsaDeMoedas, 526;
    set @TeiaDeAraha, 3146;
    set @OvoDeCobraDaMontanha, 715;
    
    
    if (QUEST_BlackShield == 0 && BaseLevel < 70) goto L_SemLevel;
    if (QUEST_BlackShield == 1) goto L_Retorno;
    if (QUEST_BlackShield == 2) goto L_Fim;
goto L_IniciaQuest;

L_SemLevel:
    mes "[Aquiles]";
    mes "\"O que você faz aqui?\"";
    next;
    mes "[Aquiles]";
    mes "\"Este lugar é muito perigoso para você, sai daqui imediatamente!\"";
close;

L_IniciaQuest:
    mes "[Aquiles]";
    mes "\"Nós fomos os maiores enquanto existimos. Paramos em nosso auge.\"";
    next;
    mes "[Aquiles]";
    mes "\"Opa! nem vi você ai o que quer?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
    "Hein? De quem você está falando? Á quem se refere quando diz nós?", L_Hein,
    "Você é louco! Vou sair daqui!", L_Louco;
close;

L_Louco:
    mes "";
    mes "[Aquiles]";
    mes "\"Uma vida de assassinatos deixa qualquer um louco.\"";
close;

L_Hein:
    mes "";
    mes "[Aquiles]";
    mes "Eu me refiro ao meu Clãn. Nós eramos conhecidos como Escudos Negros.";
    mes "\"Pois nós usavamos escudos negros.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
    "Por favor, conte mais sobre seu Clãn.", L_Conte,
    "E quem me garante que isso é verdade? Tenho mais oque fazer, até mais!", -;
    mes "";
    mes "[Aquiles]";
    mes "\"Ok!\"";
close;

L_Conte:
    mes "";
    mes "[Aquiles]";
    mes " Nós ja fomos o clãn mais forte e rico de todo o mundo. Mas tudo um dia acaba. ";
    mes "Fomos pegos em uma emboscada. Eu e os escudos de meus companheiros fomos os unicos que sobraram.";
    mes "\"Você parece ser jovem e saudável pode, ajudar um idoso com umas coisinhas?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
    "Quais coisinhas são essas?", L_Coisas,
    "Vish, me desculpe, agora não dá. Até mais!", L_Depois;
close;

L_Depois:
    mes "";
    mes "[Aquiles]";
    mes "\"Até.\"";
close;

L_Coisas:
    mes "";
    mes "[Aquiles]";
    mes "\"Preciso que pegue estas coisas:";
    mes " *  600 Bolotas.";
    mes " *  850 Casulos de Seda.";
    mes " *  1000 Cristais Escuro.";
    mes " *  4000 Gosmas de Verme.";
    mes " *  25 Escamas de Dragão.";
    mes " *  300 Chaves do Tesouro.";
    mes " *  60 Balinhas.";
    mes " *  120 Bolsas de Moedas.";
    mes " *  25 Teias de Aranha.";
    mes " *  40 Ovos de Cobras da Montanha.\"";    
    next;
    mes "[" + strcharinfo(0) + "]";
    menu 
    "Ok! Já volto com essas coisas.", L_Aceitar,
    "Deixa para outro dia!",-;
    mes "";
    mes "[Aquiles]";
    mes "=S \"Tá bom então.\"";
close;

L_Aceitar:
    mes "";
    mes "[Aquiles]";
    mes "\"Ok! Ficarei esperando!\"";
    set QUEST_BlackShield, 1;
close;
   
L_Retorno:
    mes "[Aquiles]";
    if (Sex == 1) mes "\"Olá meu amigo, "+strcharinfo(0)+"! Trouxe os itens que eu te pedi?\"";
    if (Sex == 0) mes "\"Olá minha amiga, "+strcharinfo(0)+"! Trouxe os itens que eu te pedi?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu 
    "Aqui estão!", -,
    "Esqueci o que você precisa!", L_ItensNecessarios;
    if (countitem(@Bolota)<600 || countitem(@CasuloDeSeda)<850 || countitem(@CristalEscuro)<1000 || countitem(@PataDeInseto)<2000 || countitem(@GosmaDeVerme)<4000 || countitem(@EscamasDeDragao)<25 || countitem(@ChaveDoTesouro)<300 || countitem(@Balinha)<60 || countitem(@BolsaDeMoedas)<120 || countitem(@TeiaDeAraha)<25 || countitem(@OvoDeCobraDaMontanha)<40 ) goto L_naotem;
    getinventorylist;
    if (@inventorylist_count >=100) goto L_SemEspaco;
    delitem @Bolota, 600;
    delitem @CasuloDeSeda, 850;
    delitem @CristalEscuro, 1000;
    delitem @PataDeInseto, 2000;
    delitem @GosmaDeVerme, 4000;
    delitem @EscamasDeDragao, 25;
    delitem @ChaveDoTesouro, 300;
    delitem @Balinha, 60;
    delitem @BolsaDeMoedas, 120;
    delitem @TeiaDeAraha, 25;
    delitem @OvoDeCobraDaMontanha, 40;
    getitem @EscudoNegro, 1;
    set QUEST_BlackShield, 2;
    mes "";
    mes "[Aquiles]";
    mes "=) \"Muito bem! Tudo que eu pedi esta aqui.\"";
    next;
    mes "[Aquiles]";
    mes "=D \"Tome este escudo. Este pertencia a um dos meus companheiros de Clãn\"";
close;

L_naotem:
    mes "";
    mes "[Aquiles]";
    mes "\"Estão faltando algumas coisas! Volte quando tiver tudo que eu te pedi.\"";
close;

L_ItensNecessarios:
    mes "";
    mes "[Aquiles]";
    mes "\"Deseja que eu diga novamente os nomes dos itens que eu vou precisar?\"";
    next;    
    mes "[" + strcharinfo(0) + "]";
    mes "\"Sim!\"";
    next;    
    mes "[Aquiles]";
    mes "\"São estes:";
    mes " *  600 Bolotas.";
    mes " *  850 Casulos de Seda.";
    mes " *  1000 Cristais Escuro.";
    mes " *  4000 Gosmas de Verme.";
    mes " *  25 Escamas de Dragão.";
    mes " *  300 Chaves do Tesouro.";
    mes " *  60 Balinhas.";
    mes " *  120 Bolsas de Moedas.";
    mes " *  25 Teias de Aranha.";
    mes " *  40 Ovos de Cobras da Montanha.\"";    
    next;
    mes "[Aquiles]";
    mes "=D \"Não esqueça!\"";
close;

L_SemEspaco:
    mes "";
    mes "[Aquiles]";
    if (Sex == 1) mes "\"Volte depois meu amigo.\"";
    if (Sex == 0) mes "\"Volte depois minha amiga.\"";
    close;
    mes "[Aquiles]";
    mes "\"Você está com muitas coisas em sua mochila. O escudo não cabe. Volte quando estiver com algum espaço.\"";
close;

L_Fim:
    mes "[Aquiles]";
    mes "\"Use o escudo negro e espalhe nosso nome por todo o mundo mais uma vez!.\"";
close;

}

// Placa da ponte que leva ao 010-4
// Por Xtreem
// Vou usar esta placa em uma quest de deve envolver um bucado de npc.
// Ainda não decidi quantos.
//

010,25,68,0|script|#PlacaVelha|300
{
    mes "[Placa]";
    mes "Esta ponte foi construida a mando do soberano de tudo e de todos!";
    mes "O Rei Gelido!";
    next;
    mes "[Placa]";
    mes "*Você olha para  a placa e percebe que ela é feita de uma madeira muito mais velha que a ponte.*";
    mes "*Mas decide não fazer nada e segue seu caminho*";
    close;
}

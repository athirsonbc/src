// This NPC makes potions and dyes
// Tradução por Fafinha
// Adaptação para o server BR por alastrim

// Script debug para testar o funcionamento do NPC Raquel.
//010,130,57,0|script|debug|103
//{
//    menu
//        "Reset",              L_reset,
//        "Reset verde escuro", L_reset2,
//        "Ganhar materiais",   L_ganhar,
//        "Sair",               L_Fechar;
//L_reset:
//    set QUEST_tinta, 1;
//    set QUEST_MASK1, QUEST_MASK1 & ~MASK1_SABETINTA; // seta 0 apenas para MASK1_SABETINTA
//    close;
//L_reset2:
//    set QUEST_MASK1, QUEST_MASK1 & ~MASK1_SABETINTA2; // seta 0 apenas para MASK1_SABETINTA2
//    close;
//L_ganhar:
//    getitem "ErvaMauva", 100;
//    getitem "ErvaCobalto", 100;
//    getitem "ErvaGamboge", 100;
//    getitem "ErvaAlizarina", 100;
//    close;
//L_Fechar:
//   close;
//}

010,132,57,0|script|Raquel|103
{
    set @EscolhaIntroTingimento, 0;
    set @EscolhaCorante, 1;
    set @EscolhaAbortar, 2;

    setarray @menuitems$, "", "", "", "", "";
    set @c, 0;

    set @menuitems$[@c], "Estou muito ocupad" + @fm$ + ". Depois eu volto.";
    set @menuID[@c], @EscolhaAbortar;
    set @c, @c + 1;

    mes "[Raquel a Alquimista]";
    mes "\"Estou aprendendo a antiga ciência da alquimia. Estou estudando bastante para conseguir criar vários tipos de poções!\"";
    if (QUEST_MASK1 & MASK1_SABETINTA) goto L_skip_introducing;
    if (QUEST_tinta == 1) goto L_pre_dyeing;
    close;

L_skip_introducing:
    next;
    mes "[Raquel a Alquimista]";
    mes "\"Olá, bem-vind" + @fm$ + " de volta! Você está interessado em mais tintas?\"";
    set @menuitems$[@c], "Sim. Tintas, por favor.";
    set @menuID[@c], @EscolhaCorante ;
    set @c, @c + 1;
    goto L_main_menu;

L_pre_dyeing:
    set @menuitems$[@c], "Você sabe fazer tintas?";
    set @menuID[@c], @EscolhaIntroTingimento;
    set @c, @c + 1;
    goto L_main_menu;

L_main_menu:
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        @menuitems$[0], L_Next,
        @menuitems$[1], L_Next,
        @menuitems$[2], L_Next,
        @menuitems$[3], L_Next,
        @menuitems$[4], L_Next,
        @menuitems$[5], L_Next;

L_Next:
    set @menu, @menu - 1;

    if (@menu >= @c) goto L_abort;
    if (@menuID[@menu] == @EscolhaIntroTingimento) goto L_dyeing_intro;
    if (@menuID[@menu] == @EscolhaCorante ) goto L_pick_colour;
    if (@menuID[@menu] == @EscolhaAbortar) goto L_abort;
    goto L_abort;

//---------------------------------------------------------------------

L_dyeing_intro:
    mes "";
    mes "[Raquel a Alquimista]";
    mes "Raquel para por um instante.";
    mes "\"Hmm, eu não sou muito boa nisso... Só posso fazer tintas simples, daquelas que se usam em algodão e lã.\"";
    next;
    mes "[Raquel a Alquimista]";
    mes "\"Para as tintas realmente boas eu precisaria de um poderoso catalizador, como um Spork Obsidian ou um Wumpus Egg...\"";
    mes "Raquel parece viajar em seus pensamentos.";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Eu só queria pintar roupas...", L_Pintar,
        "O que é um 'Wumpus Egg'?",      L_wumpus_egg_intro,
        "O que é um 'Obsidian Spork'?",  L_obsidian_spork_intro,
        "Er, não faz mal...",            L_abort;

L_Pintar:
    mes "";
    mes "[Raquel a Alquimista]";
    mes "\"Oh, apenas para roupas? Isso é fácil. Eu posso fazer nas cores vermelha, amarela, azul, laranja, rosa, verde, azul escura, preta e roxa, desde que você me consiga os ingredientes para isso.\"";
    next;
    set QUEST_tinta, 0;
    set QUEST_MASK1, (QUEST_MASK1 | MASK1_SABETINTA);
    goto L_pick_colour;

L_wumpus_egg_intro:
    mes "[Raquel a Alquimista]";
    mes "Os olhos de Raquel brilham.";
    mes "\"Ah, o Wumpus Egg é um dos catalizadores mais poderosos que existe. Infelizmente o Wumpus está praticamente extinto atualmente, dessa forma ele é muito raro.";
    mes "Se você encontrar algum, por favor me avise. Pagarei muito bem por ele!\"";
    close;

L_obsidian_spork_intro:
    mes "[Raquel a Alquimista]";
    mes "Raquel coça a cabeça.";
    mes "\"Hmm, obsidian é um material escuro parecido com vidro, normalmente encontrado perto de vulcões. Moldá-lo não é muito difícil, mas fazer isso sem que ele perca suas propriedades mágicas é quase impossível.\"";
    next;
    mes "[Raquel a Alquimista]";
    mes "\"Obsidian sporks são então muito difíceis de fazer, mas dizem que é muito útil o seu uso na alquimia.";
    mes "Se você encontrar um por favor me avise!\"";
    close;

L_pick_colour:
    mes "";
    mes "[" + strcharinfo(0) + "]";
    menu
        "Quais ingredientes você precisa?", L_description,
        "Vermelho",                         L_red,
        "Amarelo",                          L_yellow,
        "Azul claro",                       L_light_blue,
        "Verde",                            L_green,
        "Laranja",                          L_orange,
        "Rosa",                             L_pink,
        "Azul escuro",                      L_dark_blue,
        "Preto",                            L_black,
        "Roxo",                             L_purple,
        "Verde escuro",                     L_d_dark_green_quest,
        "Na verdade nada.",                 L_abort;

L_description:
    mes "";
    mes "[Raquel a Alquimista]";
    mes "\"As tintas que conheço são feitas a partir de ervas. Alizarina para o vermelho, gamboge para o amarelo, cobalto para o azul e malva como agente de ligação para as cores escuras.\"";
    next;
    mes "[Raquel a Alquimista]";
    mes "\"Algumas das cores precisam de mais ingredientes, incluindo algumas que você não encontra aqui, então eu tenho que cobrar por eles.";
    next;
    mes "[Raquel a Alquimista]";
    mes "Oh, e para o azul escuro e o roxo eu também preciso de uma pérola, sem pó de pérola não fica uma cor brilhante.\"";
    next;
    goto L_description_quick;

L_description_quick:
    mes "[" + strcharinfo(0) + "]";
    menu
        "O que você precisa para a tinta vermelha?",     L_d_red,
        "O que você precisa para a tinta amarelo?",      L_d_yellow,
        "O que você precisa para a tinta azul claro?",   L_d_light_blue,
        "O que você precisa para a tinta verde?",        L_d_green,
        "O que você precisa para a tinta laranja?",      L_d_orange,
        "O que você precisa para a tinta rosa?",         L_d_pink,
        "O que você precisa para a tinta azul escuro?",  L_d_dark_blue,
        "O que você precisa para a tinta preto?",        L_d_black,
        "O que você precisa para a tinta roxa?",         L_d_purple,
        "O que você precisa para a tinta verde escuro?", L_d_dark_green,
        "Atualmente...",                                 L_Atualmente;

L_Atualmente:
    mes "";
    mes "[" + strcharinfo(0) + "]";
    menu
        "Você pode fazer alguma tinta para mim, por favor?",        L_pick_colour,
        "Onde consigo ervas?",                                      L_d_herbs,
        "Onde consigo pétalas?",                                    L_d_petals,
        "Onde encontro minério de ferro?",                          L_d_ore,
        "Onde encontro pérolas?",                                   L_d_pearl,
        "Onde encontro gosma de verme?",                            L_d_maggot_slime,
        "Estou vendo. Obrigad" + @fm$ + " e um bom dia para você!", L_abort;

L_d_herbs:
    mes "[Raquel a Alquimista]";
    mes "\"Arbustos de alizarina, mauva, cobalto e gamboge crescem por aí. Eu não tenho visto aqui em volta, mas se você procurar, tenho certeza de que vai encontrar algumas.\"";
    next;
    goto L_description_quick;

L_d_petals:
    mes "[Raquel a Alquimista]";
    mes "\"Basta pegar de algumas flores por ai. Mas cuidado que elas não gostam que toquem nelas.\"";
    next;
    goto L_description_quick;

L_d_ore:
    mes "[Raquel a Alquimista]";
    mes "\"Minério de ferro pode ser encontrado em algumas minas por ai. É onde as pessoas costumam encontrar.\"";
    next;
    goto L_description_quick;

L_d_pearl:
    mes "[Raquel a Alquimista]";
    mes "\"Uma pérola ... hmm, pode ser um pouco complicado. Você só encontra no fundo do mar, mas dizem os piratas possuem algumas.\"";
    next;
    goto L_description_quick;

L_d_maggot_slime:
    mes "[Raquel a Alquimista]";
    mes "\"Como o nome sugere, gosma de verme é mais encontrado a partir de vermes. Existe outras criaturas, como o morcego, que secreta uma gosma semelhante que para a alquimia funciona igual.\"";
    next;
    goto L_description_quick;

L_intermediate:
    mes "[" + strcharinfo(0) + "]";
    menu "Você pode fazer uma tinta para mim?", L_pick_colour,
        "Você pode descrever os ingredientes?", L_description_quick,
        "Nem pensar.",                          L_abort;

L_ok:
    mes "";
    mes "[Raquel a Alquimista]";
    mes "\"Aqui está a sua tinta. Lembre-se de usar tudo, ou a cor pode ficar desbotada.\"";
    next;
    goto L_pick_colour;

L_red:
    if (countitem("ErvaAlizarina") < 10) goto L_red_fail;
    getinventorylist;
    if (@inventorylist_count == 100 && countitem("ErvaAlizarina") > 10) goto L_TooMany;
    delitem "ErvaAlizarina", 10;
    getitem "TintaVermelha", 1;
    goto L_ok;

L_red_fail:
    mes "[Raquel a Alquimista]";
    mes "\"Hmm, desculpe, mas eu preciso de 10 [" + getitemlink("ErvaAlizarina") + "] para fazer a tinta vermelha.\"";
    next;
    goto L_intermediate;

L_d_red:
    mes "";
    mes "[Raquel a Alquimista]";
    mes "\"Tinta vermelha é fácil, basta me trazer 10 [" + getitemlink("ErvaAlizarina") + "] que eu preparo imediatamente.\"";
    next;
    goto L_intermediate;

L_yellow:
    if (countitem("ErvaGamboge") < 10) goto L_yellow_fail;
    getinventorylist;
    if (@inventorylist_count == 100 && countitem("ErvaGamboge") > 10) goto L_TooMany;
    delitem "ErvaGamboge", 10;
    getitem "TintaAmarela", 1;
    goto L_ok;

L_yellow_fail:
    mes "[Raquel a Alquimista]";
    mes "\"Não há " + getitemlink("ErvaGamboge") + " o bastante, eu preciso de pelo menos 10. Não posso preparar a tinta para você agora.\"";
    next;
    goto L_intermediate;

L_d_yellow:
    mes "[Raquel a Alquimista]";
    mes "\"Tinta amarela nada mais é que o extrato da folha de gamboge. Se você me conseguir 10 [" + getitemlink("ErvaGamboge") + "], posso facilmente prepará-lo para você.\"";
    next;
    goto L_intermediate;

L_light_blue:
    if (countitem("ErvaCobalto") < 10) goto L_light_blue_fail;
    getinventorylist;
    if (@inventorylist_count == 100 && countitem("ErvaCobalto") > 10) goto L_TooMany;
    delitem "ErvaCobalto", 10;
    getitem "TintaAzulClara", 1;
    goto L_ok;

L_light_blue_fail:
    mes "[Raquel a Alquimista]";
    mes "\"Desculpe, mas não posso fazer isso para você por menos de 10 [" + getitemlink("ErvaCobalto") + "].\"";
    next;
    goto L_intermediate;

L_d_light_blue:
    mes "";
    mes "[Raquel a Alquimista]";
    mes "\"Tinta azul claro é o resultado de se deixar 10 folhas de cobalto misturadas em álcool por uma noite. Eu já tenho alguns aqui e posso trocar por 10 [" + getitemlink("ErvaCobalto") + "].\"";
    next;
    goto L_intermediate;

L_green:
    if (countitem("ErvaCobalto") < 20 || countitem("ErvaGamboge") < 20 || Zeny < 1000) goto L_green_fail;
    getinventorylist;
    if (@inventorylist_count == 100 && countitem("ErvaCobalto") > 20 && countitem("ErvaGamboge") > 20) goto L_TooMany;
    delitem "ErvaCobalto", 20;
    delitem "ErvaGamboge", 20;
    set Zeny, Zeny - 1000;
    getitem "TintaVerde", 1;
    goto L_ok;

L_green_fail:
    mes "[Raquel a Alquimista]";
    mes "\"Não... Eu preciso de muito cobalto e gamboge para preparar a tinta.\"";
    mes "\"20 [" + getitemlink("ErvaGamboge") + "], 20 [" + getitemlink("ErvaCobalto") + "] e 1.000 GP deve ser o suficiente.\"";
    next;
    goto L_intermediate;

L_d_green:
    mes "";
    mes "[Raquel a Alquimista]";
    mes "\"Tinta verde é um pouco mais complicado de se fazer, você precisa ferver as folhas de cobalto na temperatura certa em um tubo de cobalto.";
    mes "Fazer um tubo de cobalto não é fácil e sempre que tento acabo perdendo um tubo de cristal...\"";
    next;
    mes "[Raquel a Alquimista]";
    mes "\"Assim, vou precisar de 20 [" + getitemlink("ErvaGamboge") + "], 20 [" + getitemlink("ErvaCobalto") + "] e 1.000 GP para os materiais.";
    next;
    goto L_intermediate;

L_orange:
    if (countitem("ErvaAlizarina") < 10 || countitem("ErvaGamboge") < 10 || countitem("MinerioDeFerro") < 2 || Zeny < 1000) goto L_orange_fail;
    getinventorylist;
    if (@inventorylist_count == 100 && countitem("ErvaAlizarina") > 10 && countitem("ErvaGamboge") > 10 && countitem("MinerioDeFerro") > 2) goto L_TooMany;
    delitem "ErvaAlizarina", 10;
    delitem "ErvaGamboge", 10;
    delitem "MinerioDeFerro", 2;
    set Zeny, Zeny - 1000;
    getitem "TintaLaranja", 1;
    goto L_ok;

L_orange_fail:
    mes "[Raquel a Alquimista]";
    mes "\"Me desculpe, mas eu preciso de 2 [" + getitemlink("MinerioDeFerro") + "], 10 [" + getitemlink("ErvaGamboge") + "], 10 [" + getitemlink("ErvaAlizarina") + "], e 1.000 GP cada uma.\"";
    next;
    goto L_intermediate;

L_d_orange:
    mes "";
    mes "[Raquel a Alquimista]";
    mes "\"Tinta laranja é divertido de se fazer, mas requer ferro fundido e um pouco de pó de enxofre, o que não é facil de se conseguir.";
    next;
    mes "[Raquel a Alquimista]";
    mes "\"Ainda assim se me trouxer 10 [" + getitemlink("ErvaAlizarina") + "], 10 [" + getitemlink("ErvaGamboge") + "], 2 [" + getitemlink("MinerioDeFerro") + "] e 1.000 GP, eu posso preparar a tinta para você.\"";
    next;
    goto L_intermediate;

L_pink:
    if (countitem("ErvaAlizarina") < 10 || countitem("Petala") < 6 || Zeny < 1000) goto L_pink_fail;
    getinventorylist;
    if (@inventorylist_count == 100 && countitem("ErvaAlizarina") > 10 && countitem("Petala") > 6) goto L_TooMany;
    delitem "ErvaAlizarina", 10;
    delitem "Petala", 6;
    set Zeny, Zeny - 1000;
    getitem "TintaRosa", 1;
    goto L_ok;

L_pink_fail:
    mes "[Raquel a Alquimista]";
    mes "\"Não, você não tem tudo que preciso, que seriam 10 [" + getitemlink("ErvaAlizarina") + "], 6 [" + getitemlink("Petala") + "] e 1.000 GP.\"";
    next;
    goto L_intermediate;

L_d_pink:
    mes "";
    mes "[Raquel a Alquimista]";
    mes "\"Tinta rosa .... Essa é engraçada, você não consegue um rosa usando as folhas habituais. Você precisa de 10 [" + getitemlink("ErvaAlizarina") + "] e além disso de 6 [" + getitemlink("Petala") + "].";
    next;
    mes "[Raquel a Alquimista]";
    mes "Para extrair corretamente o rosa das pétalas, precisa fervê-las. Preciso cobrar 1.000 GP por isso.\"";
    next;
    goto L_intermediate;

L_dark_blue:
    if (countitem("ErvaCobalto") < 100 || countitem("ErvaMauva") < 50 || countitem("Perola") < 1 || Zeny < 10000) goto L_dark_blue_fail;
    getinventorylist;
    if (@inventorylist_count == 100 && countitem("ErvaCobalto") > 100 && countitem("ErvaMauva") > 50 && countitem("Perola") > 1) goto L_TooMany;
    delitem "ErvaCobalto", 100;
    delitem "ErvaMauva", 50;
    delitem "Perola", 1;
    set Zeny, Zeny - 10000;
    getitem "TintaAzulEscura", 1;
    goto L_ok;

L_dark_blue_fail:
    mes "[Raquel a Alquimista]";
    mes "\"Vou precisar de todos os ingredientes para fazer a tinta azul escuro para você.\"";
    mes "\"Preciso de 100 [" + getitemlink("ErvaCobalto") + "], 50 [" + getitemlink("ErvaMauva") + "], 1 [" + getitemlink("Perola") + "] e 10.000 GP.\"";
    next;
    goto L_intermediate;

L_d_dark_blue:
    mes "";
    mes "[Raquel a Alquimista]";
    mes "\"Azul escuro é uma das cores mais complicadas. Você precisa de um extrato concentrado de 100 [" + getitemlink("ErvaCobalto") + "] e 50 [" + getitemlink("ErvaMauva") + "] para a ligação adequada.";
    next;
    mes "[Raquel a Alquimista]";
    mes "Para dar o brilho. uma [" + getitemlink("Perola") + "] é indispensável, e a parte mais cara é um pó de safira que preciso cobrar 10.000 GP por ele.\"";
    next;
    goto L_intermediate;

L_purple:
    if (countitem("ErvaCobalto") < 100 || countitem("ErvaAlizarina") < 100 || countitem("ErvaMauva") < 20 || countitem("Perola") < 1 || Zeny < 40000) goto L_purple_fail;
    getinventorylist;
    if (@inventorylist_count == 100 && countitem("ErvaCobalto") > 100 && countitem("ErvaAlizarina") > 100 && countitem("ErvaMauva") > 20 && countitem("Perola") > 1) goto L_TooMany;
    delitem "ErvaCobalto", 100;
    delitem "ErvaAlizarina", 100;
    delitem "ErvaMauva", 20;
    delitem "Perola", 1;
    set Zeny, Zeny - 40000;
    getitem "TintaRoxa", 1;
    goto L_ok;

L_purple_fail:
    mes "[Raquel a Alquimista]";
    mes "\"Não... Acho que você não tem todos os ingredientes que preciso, deixe eu ver...\"";
    mes "\"Eu preciso de 100 [" + getitemlink("ErvaCobalto") + "], 100 [" + getitemlink("ErvaAlizarina") + "], 20 [" + getitemlink("ErvaMauva") + "], 1 [" + getitemlink("Perola") + "] e de 40.000 GP.\"";
    next;
    goto L_intermediate;

L_d_purple:
    mes "[Raquel a Alquimista]";
    mes "\"Hmm, tinta roxa... alguns dos ingredientes são muito caros.";
    mes "Eu não posso lhe dizer o por que, já que são segredos da alquimia, e eu não quero discutir isso com pessoas que podem me fazer de palhaço.\"";
    next;
    mes "[Raquel a Alquimista]";
    mes "\"Ainda assim, eu posso fazer isso para você, só preciso de 100 [" + getitemlink("ErvaCobalto") + "], 100 [" + getitemlink("ErvaAlizarina") + "], 20 [" + getitemlink("ErvaMauva") + "], 1 [" + getitemlink("Perola") + "] e 40.000 GP. O dinheiro será usado para os demais ingredientes.\"";
    next;
    goto L_intermediate;

L_black:
    if (countitem("ErvaCobalto") < 40 || countitem("ErvaAlizarina") < 40 || countitem("ErvaGamboge") < 40 || countitem("ErvaMauva") < 40 || Zeny < 20000) goto L_black_fail;
    getinventorylist;
    if (@inventorylist_count == 100 && countitem("ErvaCobalto") > 40 && countitem("ErvaAlizarina") > 40 && countitem("ErvaGamboge") > 40 && countitem("ErvaMauva") > 40) goto L_TooMany;
    delitem "ErvaCobalto", 40;
    delitem "ErvaAlizarina", 40;
    delitem "ErvaGamboge", 40;
    delitem "ErvaMauva", 40;
    set Zeny, Zeny - 20000;
    getitem "TintaPreta", 1;
    goto L_ok;

L_black_fail:
    mes "[Raquel a Alquimista]";
    mes "\"Me desculpe mas você não tem todos os ingredientes que preciso para fazer a tinta preta. Eu preciso de 40 folhas de [" + getitemlink("ErvaCobalto") + "], [" + getitemlink("ErvaAlizarina") + "], [" + getitemlink("ErvaGamboge") + "], [" + getitemlink("ErvaMauva") + "] cada uma, e mais 20.000 GP.\"";
    next;
    goto L_intermediate;

L_d_black:
    mes "";
    mes "[Raquel a Alquimista]";
    mes "\"Preto? Ah! Uma bela escolha, é minha cor preferida! Bem, se você também quiser ficar bonito como eu, eu ficaria feliz em lhe preparar uma tinta preta. Apenas me traga 40 folhas cada uma, de [" + getitemlink("ErvaCobalto") + "], [" + getitemlink("ErvaAlizarina") + "], [" + getitemlink("ErvaGamboge") + "] e [" + getitemlink("ErvaMauva") + "].\"";
    next;
    mes "[Raquel a Alquimista]";
    mes "\"Ah, e 20.000 GP para a mistura de carvão e pó de diamante que precisa para tirar o cheiro da malva.\"";
    next;
    goto L_intermediate;


L_dark_green:
    if (countitem("ErvaCobalto") < 10 || countitem("ErvaGamboge") < 10 || countitem("ErvaMauva") < 10 || countitem("GosmaDeVerme") < 1 || Zeny < 1000) goto L_dark_green_fail;
    getinventorylist;
    if (@inventorylist_count == 100 && countitem("ErvaCobalto") > 10 && countitem("ErvaGamboge") > 10 && countitem("ErvaMauva") > 10 && countitem("GosmaDeVerme") > 1) goto L_TooMany;
    delitem "ErvaCobalto", 10;
    delitem "ErvaMauva", 10;
    delitem "ErvaGamboge", 10;
    delitem "GosmaDeVerme", 1;
    set Zeny, Zeny - 1000;
    getitem "TintaVerdeEscura", 1;
    goto L_ok;

L_dark_green_fail:
    mes "[Raquel a Alquimista]";
    mes "Raquel a testa enquanto analisa os ingredientes.";
    mes "\"Não, isso não serve. Eu preciso de 10 [" + getitemlink("ErvaGamboge") + "], 10 [" + getitemlink("ErvaCobalto") + "], 10 [" + getitemlink("ErvaMauva") + "], 1 [" + getitemlink("GosmaDeVerme") + "] e 1.000 GP.\"";
    next;
    goto L_intermediate;

L_d_dark_green:
    mes "";
    mes "[Raquel a Alquimista]";
    mes "\"Além de saber exatamente quantas miligramas de pó fixador é necessário para você, tudo que precisamos é de 10 folhas cada uma de [" + getitemlink("ErvaMauva") + "], [" + getitemlink("ErvaCobalto") + "] e [" + getitemlink("ErvaGamboge") + "], 1 [" + getitemlink("GosmaDeVerme") + "], e 1.000.GP.\"";
    next;
    goto L_intermediate;

L_d_dark_green_quest:
    if (QUEST_MASK1 & MASK1_SABETINTA2) goto L_dark_green;
    mes "[Raquel a Alquimista]";
    mes "\"Ah, verde escuro... tinta verde escuro é até simples, exceto por um pequeno detalhe, a roupa desbota muito facilmente quando em contato com a pele humana. A melhor forma de resolver isso é usar um pó fixador, misturado com a essência extraída da gosma de verme.\"";
    next;
    mes "[Raquel a Alquimista]";
    mes "\"Mas quanto do fixador é necessário vária de indivíduo para indivíduo. Temos que descobrir exatamente quantas miligramas do fixador é preciso usar no seu caso antes de preparar a tinta verde escuro.\"";
    next;
    set QUEST_fixadorValor_green, (rand(999));
    goto L_d_dark_green_q_main;

L_d_dark_green_q_main:
    mes "[" + strcharinfo(0) + "]";
    menu
        "Hmm... não obrigad" + @fm$ + ".",                     L_intermediate,
        "Então ninguém mais pode usar a roupa que eu pintar?", L_dark_green_q_wear,
        "OK, o que preciso fazer?",                            L_dark_green_q_explain,
        "Vamos descobrir isso!",                               L_dark_green_q_guess_0;

L_dark_green_q_wear:
    mes "[Raquel a Alquimista]";
    mes "\"A cor estabiliza uma semana depois. Outras pessoas poderão utilizá-la, mas tenha certeza de que já passou o tempo para estabilizar a cor.\"";
    next;
    goto L_d_dark_green_q_main;

L_dark_green_q_explain2:
    mes "[Raquel a Alquimista]";
    mes "Raquel pisca.";
    mes "\"Muito bem. Funciona assim, você me dá uma gosma de verme e me diz quantas miligramas vão entrar na mistura da tinta.\"";
    next;
    mes "[Raquel a Alquimista]";
    mes "\"Então, tentamos descobrir se essa era a quantidade certa. Eu vou lhe dizer se acertou ou não, em caso de erro eu digo se foi para mais ou para menos.\"";
    next;
    mes "[Raquel a Alquimista]";
    mes "\"Com bastante gosmas de verme podemos descobrir o quanto você precisa.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Er... você pode explicar de novo?", L_dark_green_q_explain2,
        "Vamos lá!",                         L_dark_green_q_guess_0,
        "Eu não tenho tempo para isso.",     L_intermediate;

L_dark_green_q_explain:
    mes "";
    mes "[Raquel a Alquimista]";
    mes "\"Nós temos que verificar o equilíbrio de acidez da sua pele e fazer com que a concentração de gosma na mistura seja a ideal para as suas condições.\"";
    next;
    mes "[Raquel a Alquimista]";
    mes "\"Serão várias tentativas e para cada nova tentativa vamos precisar de uma gosma de verme.\"";
    next;
    mes "[Raquel a Alquimista]";
    mes "\"Cabe a você me dizer em três etapas quantas miligramas do fixador pretende usar. Eu então preparo a mistura e em seguida você coloca o seu dedo dentro da mistura e vemos a reação.\"";
    next;
    mes "[Raquel a Alquimista]";
    mes "\"Se a mistura ficar muito escura significa que colocamos muita gosma, se ficar muito claro significa que colocamos muito pouca e se ela manter sua cor, então acertamos a quantidade.\"";
    next;
    mes "[Raquel a Alquimista]";
    mes "Raquel sorri.";
    mes "\"Mas não se preocupe eu farei os testes totalmente de graça. Me divirto um pouco nessa brincadeira.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Er... pode explicar isso de novo?", L_dark_green_q_explain,
        "Errr... o quê?",                    L_dark_green_q_explain2,
        "Vamos logo com isso!",              L_abort,
        "Não tenho tempo para isso.",        L_intermediate;

L_dark_green_q_guess_0:
    if (countitem("GosmaDeVerme") < 1) goto L_dark_green_q_noslime;
    delitem "GosmaDeVerme", 1;
    mes "[Raquel a Alquimista]";
    mes "Raquel enche uma pequena garrafa com a gosma de verme e retira uma garrafa de vidro de dentro do seu manto.";
    next;
    mes "[Raquel a Alquimista]";
    mes "\"Agora precisamos descobrir exatamente quantas miligramas do fixador serão necessárias. Mas vamos por etapas. Primeiro me diga quantas miligramas você deseja utilizar.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Nenhuma",      L_Next2,
        "1 miligrama",  L_Next2,
        "2 miligramas", L_Next2,
        "3 miligramas", L_Next2,
        "4 miligramas", L_Next2,
        "5 miligramas", L_Next2,
        "6 miligramas", L_Next2,
        "7 miligramas", L_Next2,
        "8 miligramas", L_Next2,
        "9 miligramas", L_Next2;

L_Next2:
    set @menu, @menu - 1;
    set @guess_accumulator, @menu;
    mes "";
    mes "[Raquel a Alquimista]";
    if (@menu) mes "Raquel despeja boa parte da mistura dentro da garrafa de vidro.";
    mes "\"Temos agora " + @guess_accumulator + " miligramas no total. Agora me diga quantas centigramas você quer que eu adicione?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Nada",          L_Next3,
        "1 centigrama",  L_Next3,
        "2 centigramas", L_Next3,
        "3 centigramas", L_Next3,
        "4 centigramas", L_Next3,
        "5 centigramas", L_Next3,
        "6 centigramas", L_Next3,
        "7 centigramas", L_Next3,
        "8 centigramas", L_Next3,
        "9 centigramas", L_Next3;

L_Next3:
    set @menu, @menu - 1;
    set @guess_accumulator, @guess_accumulator + (@menu * 10);

    mes "";
    mes "[Raquel a Alquimista]";
    if (@menu) mes "Raquel despeja mais um pouco da gosma de verme dentro da garrafa de vidro.";
    mes "\"Temos agora um total de " + @guess_accumulator + " miligramas. Para finalizar, quantas decigramas você quer adicionar?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Nada",         L_Next4,
        "1 decigrama",  L_Next4,
        "2 decigramas", L_Next4,
        "3 decigramas", L_Next4,
        "4 decigramas", L_Next4,
        "5 decigramas", L_Next4,
        "6 decigramas", L_Next4,
        "7 decigramas", L_Next4,
        "8 decigramas", L_Next4,
        "9 decigramas", L_Next4;

L_Next4:
    set @menu, @menu - 1;
    set @guess_accumulator, @guess_accumulator + (@menu * 100);

    mes "[Raquel a Alquimista]";
    if (@menu) mes "Raquel despeja mais um pouco da gosma de verme dentro da garrafa de vidro.";
    mes "Raquel mistura mais alguns ingredientes na garrafa, agita bem e em seguida o aquece.";
    next;

    mes "[Raquel a Alquimista]";
    mes "\"Por favor coloque seu dedo aqui.\"";
    mes "Hesitante, você coloca o dedo dentro da mistura. Ela está quente, mas agradável.";
    next;
    mes "[Raquel a Alquimista]";
    mes "Você retira o dedo.";
    next;

    if (@guess_accumulator > QUEST_fixadorValor_green)
        goto L_dark_green_q_toomuch;

    if (@guess_accumulator < QUEST_fixadorValor_green)
        goto L_dark_green_q_toolittle;

    // otherwise correct guess

    mes "Nada parece acontecer a mistura, os olhos de Raquel se iluminam.";
    mes "\"É isso! Você precisa exatamente de " + @guess_accumulator + " miligramas. Vou anotar isso...\"";
    next;
    mes "[Raquel a Alquimista]";
    mes "Raquel está radiante.";
    mes "\"Agora posso fazer a tinta verde escuro para você. Tudo que eu preciso é uma gosma de verme, dez folhas de gamboge, dez folhas de cobalto, 10 de malva e 1.000 GP para os demais ingredientes.\"";
    set QUEST_fixadorValor_green, 0;
    set QUEST_MASK1, (QUEST_MASK1 | MASK1_SABETINTA2);
    next;
    goto L_intermediate;

L_dark_green_q_toomuch:
    mes "Após alguns segundos a mistura fica escura. Raquel fica desapontado.";
    mes "\"Isso foi muito, você precisa colocar menos de " + @guess_accumulator + " miligramas, tente de novo.";
    goto L_dark_green_q_again;

L_dark_green_q_toolittle:
    mes "Quase que instantaneamente, a mistura perde toda cor e fica transparente. Raquel suspira.";
    mes "\"Isso foi muito pouco. Tente colocar mais de " + @guess_accumulator + " miligramas na próxima.";
    goto L_dark_green_q_again;

L_dark_green_q_again:
    next;
    mes "[Raquel a Alquimista]";
    mes "\"Vamos tentar de novo?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Sim!",                    L_dark_green_q_guess_0,
        "Não, para mim já basta.", L_abort;

L_dark_green_q_noslime:
    mes "";
    mes "[Raquel a Alquimista]";
    mes "\"Ah. você não tem nenhuma gosma de verme? Que pena, mas é fácil encontrar, consiga ao menos uma e podemos continuar.\"";
    next;
    goto L_intermediate;

L_abort:
    close;

L_TooMany:
    mes "";
    mes "[Raquel a Alquimista]";
    mes "Você não tem espaço para carregar mais nada.";
    close;
}

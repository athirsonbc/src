// Autor:     Jesusalva
// Revisão:   Scall
// Objetivo:  Criar um Arco do Deserto


sarab,16,11,0|script|Sarabino|156
{
    if (QUEST_SARABINO == 1) goto L_Viagem;
    if (QUEST_SARABINO == 2) goto L_ArcoDeserto; 
    if (QUEST_SARABINO == 3) goto L_ArcoDesertoCheck; 
    if (QUEST_SARABINO == 4) goto L_ACABOU;

    mes "[Sarabino]";
    mes "=) \"Olá viajante! Bem-vind" + @fm$ + " ao deserto de Sarab.\"";
    next;

    if (BaseLevel < 20) goto L_SemNivel;

    mes "[Sarabino]";
    mes "o.o \"Vejo que você é forte, talvez possa me ajudar. Eu tenho um amigo alquimista, que está fazendo estudos sobre o menir da alma.\"";
    next;
    mes "[Sarabino]";
    mes "=S \"O problema é que não tem nenhum menir da alma por aqui, e mesmo que tivesse, cortar um pedaço é crime conforme decretou o Grande Imperador do Mundo de Mana.\"";
    next;
    mes "[Sarabino]";
    mes "\"Porém talvez você possa me ajudar. Me traga 5 [" + getitemlink("FragmentoMenir") + "] e 1 [" + getitemlink("CartaMundoMana") + "] e eu irei lhe recompensar muito bem.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Eu tenho o que você me pediu.", L_CristalWarp,
        "Talvez outra hora.",            L_Fechar;

L_CristalWarp:
    if (
        (countitem("CartaMundoMana") >= 1) &&
        (countitem("FragmentoMenir") >= 5)
    ) goto L_QUESTUM;
    goto L_SemItemsQuestUM;

L_QUESTUM:
    getinventorylist;
    if (@inventorylist_count == 100) goto L_CHEIO;
    set QUEST_SARABINO, 1;

    delitem "FragmentoMenir", 5;
    delitem "CartaMundoMana", 1;    

    getitem "CristalWarpDeAreia", 1;

    mes "";
    mes "[Sarabino]";
    mes "^_^ \"Muito obrigado mesmo! Por favor, leve essa pedra como recompensa por seus esforços.\"";
    close;

L_Viagem:
    if (BaseLevel < 35) goto L_SemNivel;

    mes "[Sarabino]";
    mes "=D \"Olá, " + @my$ + " amig" + @fm$ + ". Eu pretendo viajar para Bhramir em breve, e ouvi que lá tem um mágico muito habilidoso chamado Miraj, que organiza um jogo. E bem, eu queria jogar, mas eu não tenho nenhum Bilhete Mágico.\"";
    next;
    mes "[Sarabino]";
    mes "=) \"Se você pudesse me arranjar um [" + getitemlink("BilheteMagico") + "], além de uma [" + getitemlink("GarrafaDAgua") + "] e 5 [" + getitemlink("Laranja") + "] para a viagem, serei eternamente grato.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Isso é pouco, aqui está.", L_ViagemCheck,
        "Talvez outra hora.",       L_Fechar;

L_ViagemCheck:
    if (
        (countitem("Laranja")       >= 5) &&
        (countitem("GarrafaDAgua")  >= 1) &&
        (countitem("BilheteMagico") >= 1)
    ) goto L_QUESTDOIS;
    goto L_SemItemsQuestDOIS;

L_QUESTDOIS:
    getinventorylist;
    if (@inventorylist_count == 100) goto L_CHEIO;
    set QUEST_SARABINO, 2;

    delitem "Laranja",       5;
    delitem "BilheteMagico", 1;    
    delitem "GarrafaDAgua",  1;    

    getitem "File", 10;
    set Zeny, Zeny + 5000;

    mes "";
    mes "[Sarabino]";
    mes "^_^ \"Muito obrigado mesmo! Por favor, venha almoçar comigo, deve ter dado trabalho essa correria toda.\"";
    close;

L_ArcoDeserto:
    if (BaseLevel < 50) goto L_SemNivel;

    mes "[Sarabino]";
    mes "=D \"Olá, " + @my$ + " amig" + @fm$ + ". Obrigado novamente pela ajuda na viagem a Bhramir. Meu amigo Alquimista que você ajudou algum tempo atrás, também quer te recompensar melhor por seus esforços, por isso, farei uma oferta que você não pode recusar.\"";
    next;
    mes "[Sarabino]";
    mes "^_^ \"Eu sei fazer um excelente [" + getitemlink("ArcoDoDeserto") + "], o melhor arco que você já viu e jamais irá ver. Pórem ele requer uma gama de materiais muito raros. Felizmente, estamos dispostos a trocar esses materiais extremamente raros por outros que não são tão raros assim.\"";
    next;
    mes "[Sarabino]";
    mes "o.o \"Meu amigo alquimista irá providenciar as Gemas de Diamante e de Safira para construção. Porém ele quer 1 [" + getitemlink("Osso") + "], 2 [" + getitemlink("CuboGelo") + "] e 20 [" + getitemlink("GosmaAreia") + "] para estudos pessoais.\"";
    next;
    mes "[Sarabino]";
    mes "w.w \"Eu irei pegar o prisma necessário para construção em halicarnazo, por isso preciso de 1 [" + getitemlink("CristalWarpDeAreia") + "] e 50 [" + getitemlink("Leite") + "].\"";
    next;
    mes "[Sarabino]";
    mes "^.^ \"E finalmente para construção em si você irá precisar de 3 [" + getitemlink("GarrafaComAreia") + "] para os mecanismos internos, 15 [" + getitemlink("Raiz") + "] para a corda, e para o corpo do arco ainda vai precisar de uma [" + getitemlink("Perola") + "] e 5 [" + getitemlink("MinerioDeFerro") + "]. Não temos estes, desculpe.\"";
    next;
    mes "[Sarabino]";
    mes "<.< \"Também precisaremos de algo para servir como base, como o [" + getitemlink("ArcoDaFloresta") + "], quem sabe?";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "*_* O melhor arco de todos? Eu quero!!",                         L_Quero,
        ";_; Talvez em um futuro distante eu pense em uma coisa dessas.", L_MaisTarde;

L_Quero:
    set QUEST_SARABINO, 3;
    goto L_ArcoDesertoCheck;

L_ArcoDesertoCheck:
    mes "[Sarabino]";
    mes "\"Para fazer o [" + getitemlink("ArcoDoDeserto") + "] eu preciso de:\"";
    next;
    mes "* 50 [" + getitemlink("Leite") + "]";
    mes "* 20 [" + getitemlink("GosmaAreia") + "]";
    mes "* 15 [" + getitemlink("Raiz") + "]";
    mes "*  5 [" + getitemlink("MinerioDeFerro") + "]";
    mes "*  3 [" + getitemlink("GarrafaComAreia") + "]";
    mes "*  2 [" + getitemlink("CuboGelo") + "]";
    mes "*  1 [" + getitemlink("CristalWarpDeAreia") + "]";
    mes "*  1 [" + getitemlink("Osso") + "]";
    mes "*  1 [" + getitemlink("Perola") + "]";
    mes "*  1 [" + getitemlink("ArcoDaFloresta") + "]";
    mes "";
    mes "150.000 GP para os custos adicionais";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "*_* Eu tenho tudo o que você me pediu.",       L_QTRES,
        "Não se preocupe, trarei o que você me pediu.", L_Fechar;

L_QTRES:
    if (
        (countitem("Leite")              >= 50) &&
        (countitem("GosmaAreia")         >= 20) &&
        (countitem("Raiz")               >= 15) &&
        (countitem("MinerioDeFerro")     >= 5)  &&
        (countitem("GarrafaComAreia")    >= 3)  &&
        (countitem("CuboGelo")           >= 2)  &&
        (countitem("CristalWarpDeAreia") >= 1)  &&
        (countitem("Osso")               >= 1)  &&
        (countitem("Perola")             >= 1)  &&
        (countitem("ArcoDaFloresta")     >= 1)  &&
        (Zeny >= 150000)
    ) goto L_ARCODESERTO3;
    goto L_SemItemsQuestTRES;

L_ARCODESERTO3:
    getinventorylist;
    if (@inventorylist_count == 100) goto L_CHEIO;
    set QUEST_SARABINO, 4;

    delitem "Leite",              50;
    delitem "GosmaAreia",         20;
    delitem "Raiz",               15;
    delitem "MinerioDeFerro",      5;
    delitem "GarrafaComAreia",     3;
    delitem "CuboGelo",            2;
    delitem "CristalWarpDeAreia",  1;
    delitem "Osso",                1;
    delitem "Perola",              1;
    delitem "ArcoDaFloresta",      1;

    set Zeny, Zeny - 150000;
    getitem "ArcoDoDeserto", 1;
    getitem "FlechaEnvenenada", 50; // Algumas flechas enveneadas de cortesia pelo preço absurdo da quest.
    mes "[Sarabino]";
    mes "^_^ \"Aqui está seu novo arco! Eu inclui algumas flechas especiais como cortesia nossa. Parabéns, agora você tem o arco mais poderoso de todo Sarab!\"";
    close;

L_CHEIO:
    mes "[Sarabino]";
    mes "O.O \"Seu inventário está lotado! Sinto muito, mas você precisa arranjar um lugar para guardar minha recompensa.\"";      
    close;

L_SemItemsQuestUM:
    mes "";
    mes "[Sarabino]";
    mes ">.> \"Eu pedi 5 [" + getitemlink("FragmentoMenir") + "] e 1 [" + getitemlink("CartaMundoMana") + "].\"";
    close;

L_SemItemsQuestDOIS:
    mes "";
    mes "[Sarabino]";
    mes "T.T \"Eu pedi um [" + getitemlink("BilheteMagico") + "], além de uma [" + getitemlink("GarrafaDAgua") + "] e 5 [" + getitemlink("Laranja") + "] para a viagem.\"";
    close;

L_SemItemsQuestTRES:
    mes "";
    mes "[Sarabino]";
    mes "U.U \"Você não tem tudo que pedi.\"";
    close;

L_SemNivel:
    mes "[Sarabino]";
    mes "=) \"Espero que esteja gostando de nossa nobre terra.\"";
    close;

L_MaisTarde:
    mes "";
    mes "[Sabino]";
    mes "T.T \"Estarei esperando. Lembre-se: é uma oportunidade única!\"";
    close;

L_ACABOU:
    mes "[Sarabino]";
    mes "=D \"Aproveitando seu fabuloso arco novo?\"";
    close;

L_Fechar:
    close;
}

// @author  Diogo_RBG - http://diogorbg.blogspot.com/


013,120,115,0|script|Mestre em Magia|370
{
    sc_start 200, 180, 1;
    if (@pulaEufrania) goto L_pular;
    set @pulaEufrania, 1;
    mes "Você observa uma admirável senhora de seus 100 anos ou mais. E ao seu lado um enorme livro velho e rasgado.";
    next;

L_pular:
    sc_start 200,180, 1;
    mes getskilllv(SKILL_BRUXARIA);
    mes "[Eufrânia]";
    mes "\"Olá querid" + @fm$ + ". Tia Eufrânia pode ajudá-l" + @fm$ + " em algo?\"";
    menu
        "Este é um livro de magia?.",              L_livro,
        "Pode me dar o dom da bruxaria?.",         L_bruxaria,
        "Pode me ensinar bruxarias?",              L_bruxarias,
        "Retire esta bruxaria de mim!",            L_retirar,
        "Nada no momento... obrigad" + @fm$ + ".", L_nada;

L_nada:
    mes "[Eufrânia]";
    mes "\"Feliz Halloween!!!\"";
    goto L_close;

L_livro:
    sc_start 200, 180, 1;
    mes "[Eufrânia]";
    mes "\"Eu sou uma bruxa Mestre em Magia e este é meu livro. Um livro milenar que não serve nem para peso de papel... AFF!\"";
    goto L_close;

L_bruxaria:
    sc_start 200,180, 1;
    mes "* a bruxa te enrola, fala um monte de coisas e finalmente você ganha habilidades mágicas.";
    if (!(getskilllv(SKILL_BRUXARIA))) setskill SKILL_BRUXARIA, 1;
    goto L_close;

L_bruxarias:
    sc_start 200,180, 1;
    mes "[Eufrânia]";
    mes "\"Qual o tipo de bruxaria gostaria de aprender?\"";
    menu
        "Bruxaria Branca",   L_branca,
        "Bruxaria Dourada",  L_close,
        "Bruxaria Vermelha", L_close,
        "Bruxaria Negra",    L_close,
        "Melhor não ^^",     L_close;

L_branca:
    sc_start 200,180, 1;
    setarray @vet$, "-", "-";
    if (MAGIC_BRUX1 & BRUX1_APARATAR1) setarray @vet$[0], "*";

    mes "[Eufrânia]";
    mes "\"Este é um tipo de magia que não calsa mal a ninguém. Mas são muito útiteis.\"";
    menu
        "[" + @vet$[0] + "] lvl 1 - #apt - Aparatar.",         L_aparatar1,
        "[" + @vet$[1] + "] lvl 2 - #apt <local> - Aparatar.", L_bruxarias,
        "Sair.",                                               L_bruxarias;

L_aparatar1:
    sc_start 200,180, 1;
    mes "[Eufrânia]";
    mes "\"Com a magia de 'Aparatar' você poderá se teleportar à curta distância.";
    mes "Muito útil para despistar quem esteja te seguindo.\"";
    mes "";
    set @brux, 1;
    set @pag, 1;
    set @pnt, 300;
    mes "#apt, brux.lvl " + @brux + ", 10MP, 400s";
    menu
        "Aprender (" + @pag + " pag, " + @pnt + "pt).", L_continuar,
        "Não aprender.",                                L_continuar;

L_continuar:
    if (@menu == 1) set MAGIC_BRUX1, MAGIC_BRUX1|BRUX1_APARATAR1;
    goto L_bruxarias;

L_retirar:
    sc_start 200,180, 1;
    mes "* Então a magia se vai.";
    setskill SKILL_BRUXARIA, 0;
    set MAGIC_BRUX1, 0;
    //setskill SKILL_MAGIC, 0;
    //setskill 341, 0;
    //setskill 342, 0;
    //setskill 343, 0;
    //setskill 344, 0;
    //setskill 345, 0;
    //setskill 398, 0;
    //setskill 399, 0;
    //setskill 400, 0;
    //setskill 401, 0;
    //setskill 402, 0;
    //setskill 403, 0;
    //setskill 404, 0;
    //setskill 405, 0;
    goto L_close;

L_close:
    sc_start 200,10, 1;
    close;
}

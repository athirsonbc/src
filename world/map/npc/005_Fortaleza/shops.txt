005,92,67,0|shop|Jorge|120,Flecha:3,FlechaDeFerro:5,Arco:1000,ArcoDeCurtoAlcance:3000
005,105,55,0|shop|Abelardo|112,Bolo:*1,BoloDeLaranja:*1,BebidaDeCactus:*1,VermeTorrado:150
005,109,70,0|shop|Sofia|139,BoloDeCereja:100,BoloDeLaranja:*1,BebidaDeCactus:*1,VermeTorrado:150
005,92,61,0|shop|Geraldo|159,Cerveja:*1,CoxaDeFrango:575,File:*1
005,92,72,0|shop|Lara|103,BebidaDeCactus:50,PocaoDeCactus:70,PocaoEnergetica:500,PocaoDeConcentracao:500,PocaoAntiveneno:*1,PocaoMinimaDeCura:250,PocaoPequenaDeCura:500
005,108,66,0|shop|Ferraz|135,CamisaDeCorrentes:20000,ArmaduraLeve:50000,ArmaduraDoLordeDaGuerra:100000,Faca:25,FacaAfiada:100,Adaga:1000
005,96,55,0|shop|Oscar|142,Maca:25,Laranja:40
005,98,60,0|shop|#Jogador LVL 99|32767,BoneLvl99:1000000

005,98,60,0|script|Jogador LVL 99|120
{
    if (BaseLevel < 99) goto L_Fraco;
    goto L_Poderoso;

L_Poderoso:
    npctalk strnpcinfo(0), "Tenho mesmo algo para os jogadores poderosos como você!";
    goto L_Shop;
    
L_Fraco:
    set @n, rand(2);
    if (@n == 0) npctalk strnpcinfo(0), "Hmm... você não parece forte o bastante para aquilo que eu vendo.";
    if (@n == 1) npctalk strnpcinfo(0), "Sinto muito, você é fraco demais para o meu boné.";
    goto L_Shop;

L_Shop:
    shop "#Jogador LVL 99";
}

function|script|CapitaoMirc
{
    if (#GRINGO) goto L_CF;

    if (PC_DEST != 0 && !(isin("navio", 75, 23, 85, 28))) goto L_espere;
    if (PC_DEST != 0) goto L_espere2;
    if (STORY_CAPT0 == 4) goto L_QuestZero;

    mes "[Capitão Mirc]";
    mes "\"Olá " + @my$ + " jovem! Deseja viajar para alguma cidade? Meu navio pode ser lento, mas para você a viagem é de graça...\" :D";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Sim, estava mesmo precisando economizar dinheiro!", L_queroViajar,
        "Estou sem tempo, prefiro algum meio mais rápido.",  L_Fechar;

L_QuestZero:
    mes "[Capitão Mirc]";
    mes "\"Olá " + @my$ + " jovem! Deseja viajar para alguma cidade? Meu navio pode ser lento, mas para você a viagem é de graça...\" :D";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Sim, estava mesmo precisando economizar dinheiro!", L_queroViajar,
        "Estou sem tempo, prefiro algum meio mais rápido.",  L_Fechar,
        "Atualmente, não é por isso que estou aqui...", L_QuestZeroB;

L_QuestZeroB:
    mes "";
    mes "[" + strcharinfo(0) + "]";
    mes "\"Eu perdi minhas memórias. Queria saber se viajei de navio a uns... três dias atrás, talvez.\"";
    next;
    mes "[Capitão Mirc]";
    mes "\"Oras, para isso, basta consultar o registro de bordo! Espere um momento!\"";
    next;
    mes "[Capitão Mirc]";
    mes "\"Você é "+strcharinfo(0)+", não é mesmo? Está se sentindo melhor? Aqui diz que você fez duas viagens. Mas ora, como eu poderia me esquecer delas!\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "\"O que aconteceu, Capitão? Eu não me lembro de nada!\"";
    next;
    mes "[Capitão Mirc]";
    mes "\"Você tinha ido à Bhramir, isso eu tenho certeza. Bhramir é a capital do Império! De qualquer forma, você voltou acudido por uma enfermeira, todo ensanguentado. Horrível!\"";
    next;
    mes "[Capitão Mirc]";
    mes "\"Provar sua história seria realmente difícil, e a enfermeira não tinha como fazer nada além dos primeiros socorros, então, ao invés de te levar na enfermaria e arriscar um interrogatório, te deixamos do lado de fora das muralhas de sua cidade natal.\"";
    next;
    mes "[Capitão Mirc]";
    mes "\"Felizmente, após tantos anos de viagem, é fácil distinguir alguém de Froz e alguém de Halicarnazo! Haha! De qualquer forma, talvez você queira conversar com a enfermeira em Bhramir?\"";
    getexp 25, 0;
    set STORY_CAPT0, 5;
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "\"Sim, com certeza! E agora estou com curiosidade a respeito do outro reino também! Acho que vou aceitar sua oferta de uma viagem gratuita!\"";
    next;
    goto L_queroViajar;

L_queroViajar:
    mes "";
    mes "[Capitão Mirc]";
    mes "\"Excelente! Nada como relaxar durante uma calma viagem de navio, não acha? Mas me diga, para onde você pretende ir?\"";
    next;
    goto L_menuEscolha;

L_menuEscolha:
    mes "[" + strcharinfo(0) + "]";
    menu
        "Para o Polo Norte.",            L_poloNorte,
        "Para a Ilha de Bhramir.",       L_fortaleza,
        "Para as Terras Desconhecidas.", L_desconhecidas,
        "Desisti de viajar.",            L_Fechar;

L_fortaleza:
    if (@porto == 1) goto L_jaFortaleza;
    set PC_DEST, 1;
    goto L_boaViagem;

L_poloNorte:
    if (@porto == 2) goto L_jaPoloNorte;
    set PC_DEST, 2;
    goto L_boaViagem;

L_desconhecidas:
    if (@porto == 3) goto L_jaDesconhecidas;
    set PC_DEST, 3;
    goto L_boaViagem;

L_jaFortaleza:
    mes "";
    mes "[Capitão Mirc]";
    mes "\"Hahaha, mas você já está na Ilha de Bhramir! Escolha um outro destino.\"";
    next;
    goto L_menuEscolha;

L_jaPoloNorte:
    mes "";
    mes "[Capitão Mirc]";
    mes "\"Hahaha, mas você já está no Polo Norte! Escolha um outro destino.\"";
    next;
    goto L_menuEscolha;

L_jaDesconhecidas:
    mes "";
    mes "[Capitão Mirc]";
    mes "\"Hahaha, mas você já está nas terras desconhecidas! Escolha um outro destino.\"";
    next;
    goto L_menuEscolha;

L_Fechar:
    return;

L_boaViagem:
    mes "";
    mes "[Capitão Mirc]";
    if (PC_DEST == 1) mes "\"Muito bem, próximo destino da Rota do Norte: Ilha de Bhramir!\"";
    if (PC_DEST == 2) mes "\"Muito bem, próximo destino da Rota do Norte: Polo Norte!\"";
    if (PC_DEST == 3) mes "\"Muito bem, próximo destino da Rota do Norte: Terras Desconhecidas!\"";
    next;
    mes "[Capitão Mirc]";
    mes "\"Espere a próxima viagem no navio, por favor. Sairemos em breve.";
    mes "Tenha uma boa viagem!\"";
    set @waiting, 0;
    return;

L_espere:
    mes "[Capitão Mirc]";
    mes "\"Por favor, espere próximo à rampa de desembarque. Sairemos em breve.\"";
    return;

L_espere2:
    mes "[Capitão Mirc]";
    mes "\"Um pouco de paciência... Se você não sabe o que fazer olhe o mar, pode ser muito relaxante... se você não sofrer de enjoo, claro.\"";
    return;

L_CF:
	callfunc "#En-function_mirc"; // or callsub?
	end;
}

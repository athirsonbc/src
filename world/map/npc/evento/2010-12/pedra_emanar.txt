// @author Diogo_RBG - http://diogorbg.blogspot.com/


function|script|pedraEmanarAlma
{
    set @time, gettimetick(2);
    if (isin(@mapa, @x-3, @y-3, @x+3, @y+3) == 0 ) goto L_longe;

    if (countitem("FragPedraEmanarAlma") == 0 && countitem("AnelEmanarAlma") == 0) goto L_examinarPedra;
    if (countitem("FragPedraEmanarAlma") >= 1 && countitem("AnelEmanarAlma") == 0 || @possuiFragmento == 1) goto L_PossuiFragmento;
    if (getequipid(equip_misc1) != 3200) goto L_anelNaoEquipado;
    if (getequipid(equip_misc1) == 3200) goto L_tocarPedra;

    mes "[" + strcharinfo(0) + "]";
    mes "Nada acontece! O.o";
    return;

L_longe:
    mes "[Pedra de Emanar Alma]";
    mes "Muito longe... melhor chegar mais perto.";
    return;

L_examinarPedra:
    mes "[Pedra de Emanar Alma]";
    mes "Você mal pode acreditar no que vê. Uma pedra emanando uma grande quantidade de luz e energia. Você nunca tinha visto nada igual a isto.";
    mes "Você precisa saber o que é... mas o que fazer?";
    menu
        "Tocar a pedra.", L_tocar,
        "Cortar a pedra.", L_cortar,
        "Bater com um objeto pesado.", L_bater,
        "Não fazer nada!", L_Nada;

L_Nada:
    mes "[Pedra de Emanar Alma]";
    mes "\"Esta parece ser uma decisão sensata.";
    return;

L_tocar:
    mes "[Pedra de Emanar Alma]";
    mes "Você toca na pedra e sente todo o seu calor... mais nada!";
    next;
    mes "[Pedra de Emanar Alma]";
    mes "Melhor pensar em algo que faça mais efeito.";
    return;

L_cortar:
    mes "[Pedra de Emanar Alma]";
    mes "Você empunha sua espada e golpeia a pedra com toda a sua força...";
    specialeffect 28;
    next;
    mes "[Pedra de Emanar Alma]";
    mes "Faíscas voam por todos os lados, mas nada acontece com a pedra. Ela é tão resistente quanto um cristal.";
    next;
    mes "[Pedra de Emanar Alma]";
    mes "Assim você vai danificar a sua espada... melhor pensar em outra coisa.";
    return;

L_bater:
    if (@possuiFragmento == 1) goto L_PossuiFragmento;
    mes "[Pedra de Emanar Alma]";
    mes "Você bate na pedra com a ponta do cabo de sua espada e um pequeno fragmento da pedra se solta da pedra.";
    specialeffect 26;
    next;
    mes "[Pedra de Emanar Alma]";
    mes "Será que foi uma boa ideia? Bem... de qualquer forma você resolve guardar o pequeno fragmento.";
    getitem "FragPedraEmanarAlma", 1;
    set @possuiFragmento, 1;
    return;

L_PossuiFragmento:
    mes "[Pedra de Emanar Alma]";
    mes "Você ainda possui o pequeno fragmento que retirou da pedra. Não seria melhor examiná-lo antes?";
    return;

L_anelNaoEquipado:
    mes "[Pedra de Emanar Alma]";
    mes "Você toca na pedra e sente todo o seu calor... mais nada!";
    next;
    mes "[Pedra de Emanar Alma]";
    mes "Você não estaria fazendo algo errado?!";
    return;

L_tocarPedra:
    mes "[Pedra de Emanar Alma]";
    mes "Você toca na pedra e sente uma energia fluir em seu corpo ao encontro do anel. Tudo acontece numa fração de segundos.";
    set @ANEL_time, @time;
    specialeffect2 309;
    heal 0, 100, 1;
    if (ANEL_xp == 0) goto L_semXP;

    next;
    mes "[Pedra de Emanar Alma]";
    mes "A energia da pedra se estabiliza com a energia acumulada no anel e então é liberada uma grande quantidade de luz e energia. Quando isso acontece você sente suas energias sendo renovadas.";
    mes "";
    mes "* Você ganhou " + ANEL_xp + " pontos de experiência.";
    message strcharinfo(0), "Ganhei " + ANEL_xp + " pontos de experiência.";
    set BaseExp, BaseExp + ANEL_xp;
    set ANEL_xp, 0;
    return;

L_semXP:
    next;
    mes "[Pedra de Emanar Alma]";
    mes "Seu anel parece estar mais brilhante que antes... ele com certeza deve ter acumulado bastante energia.";
    return;
}

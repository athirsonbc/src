// O servidor TMW-BR está dando 8 horas de atrazo porque foi contruido para os europeus


function|script|getConvHora
{
    set @RealHora, @Valor;
    set @RealHora, @RealHora + 8; 
    if (@RealHora >= 24) set @RealHora, @RealHora - 24;
    return;
}

function|script|getConvDiaInteiro
{
    set @RealDia, @Valor;
    set @RealDia, @RealDia + 1;
    return;
}

function|script|getConvDiaTexto
{
    if (@Valor == 0) set @RealDia$, "domingo";
    if (@Valor == 1) set @RealDia$, "segunda-feira";
    if (@Valor == 2) set @RealDia$, "terça-feira";
    if (@Valor == 3) set @RealDia$, "quarta-feira";
    if (@Valor == 4) set @RealDia$, "quinta-feira";
    if (@Valor == 5) set @RealDia$, "sexta-feira";
    if (@Valor == 6) set @RealDia$, "sábado";
    return;
}

// Esse script pertence ao themanaworld.org
// Função: máquina caça-níqueis que premia o jogador pelo sorteio três números iguais
// Adaptado por: Lunovox <rui.gravata@gmail.com>


function|script|SlotMachine
{
    set @MoedasAoVencedor, 30; // Aqui fica a quantidade de moeda de cassino que o jogador ganhará caso ele sortei 3 números iguais
    goto L_inicio;

L_inicio:
    mes "[MÁQUINA CAÇA-NÍQUEIS]";
    mes "Insira a moeda de cassino e puxe a alavanca para sortear 3 números iguais e ganhar " + @MoedasAoVencedor + " vezes mais!";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Puxei a alavanca!", L_PuxarAlavanca,
        "Talvez mais tarde!", L_Fechar;

L_PuxarAlavanca:
    if (countitem("MoedasDeCassino") < 1) goto L_SemMoedas;
    delitem "MoedasDeCassino", 1;
    set @Temp1, rand(7);
    set @Temp2, rand(7);
    set @Temp3, rand(7);
    mes "";
    mes "[MÁQUINA CAÇA-NÍQUEIS]";
    mes "Os três números sorteados foram:";
    mes "   [" + @Temp1 + "] ←→ [" + @Temp2 + "] ←→ [" + @Temp3 + "]";
    next;
    
    if (@Temp1 != @Temp2 || @Temp2 != @Temp3 || @Temp1 != @Temp3) goto L_Perdeu;
    getitem "MoedasDeCassino", @MoedasAoVencedor;
    mes "[MÁQUINA CAÇA-NÍQUEIS]";
    mes "Parabéns! Você ganhou!";
    mes "Você ganha " + @MoedasAoVencedor + " Moedas de Cassino";
    next;
    goto L_inicio;
    
L_Perdeu:
    mes "[MÁQUINA CAÇA-NÍQUEIS]";
    mes "Você perdeu!";
    next;
    goto L_inicio;
    
L_SemMoedas:
    mes "";
    mes "[" + strcharinfo(0) + "]";
    mes "=S \"Não tenho Moeda de Cassino para inserir.\"";
    return;

L_Fechar:
    return;
}

// Concentração da maior parte dos scripts de monstros.


// @author  Diogo_RBG - http://diogorbg.blogspot.com
// @desc    Dropa um Bilhete Mágico quando o verme for morto.

function|script|bilheteVermeGuloso
{
    if (@bilheteVerme > 1) goto L_Return;

    getitem "BilheteMagico", 1;
    set @bilheteVerme, 2;
    message strcharinfo(0), "Consegui um Bilhete Mágico!";
    return;

L_Return:
    return;
}


// @author  Diogo_RBG - http://diogorbg.blogspot.com
// @desc    Sumona abelhas quando a colmeia for atingida.
//          arg1: @mapa$, arg2: @x, arg3: @y, arg4: @label$

function|script|sumonaAbelhas
{
    set @n, 4 - mobcount(@mapa$, @label$);
    if (@n < 1) goto L_Return;
    //if (@n > 5) set @n, 5;
    areamonster @mapa$, @x-2, @y-2, @x+2, @y+2, "", 1049, @n, @label$;
    return;

L_Return:
    return;
}

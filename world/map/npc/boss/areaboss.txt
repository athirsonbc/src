// NPC administrativo da Arena Boss
boss,0,0,0|script|PoderosoChefao|32767,0,0
{
	mes "[ADMINISTRADOR BOT-4414]";
	mes "Volte a lutar imediatamente!";
	close;

OnTimer30000:
    set $@boss$, $BOSS_name$;
    mapannounce "boss", " Próxima luta em sessenta segundos contra: "+$@boss$, 0;
    end;

OnTimer60000:
    set $@boss$, $BOSS_name$;
    mapannounce "boss", " Próxima luta em trinta segundos contra: "+$@boss$, 0;
    end;

// Certifica-se que tem jogador dentro da arena!! Nada de trapacear contra o sistema de suporte...
OnTimer75000:
    set $@inFront, getareausers("boss", 20, 20, 69, 69);
    if ($@inFront < 1) setnpctimer 0;
    if ($@inFront < 1) mapannounce "boss", "Próxima luta na Arena Boss em 1,5 minutos", 0;
    if ($@inFront < 1) mapannounce "008-2", "Próxima luta na Arena Boss em 1,5 minutos", 0;
    end;

OnTimer90000:
    // Incia Música de Boss
    //misceffect 314;
    //playbgmall "Ian_Alex_Mac_-_19_-_Clansmen.mp3", "boss"; // Antiga: sfx/bmx/epic_soul_factory_-_02_titan.ogg TODO UNTESTED 

    // Deixar em 25 por ser um mapa 50x50 ($@focoRaio precisa considerar o tile central)
    set $@focoX, 45;
    set $@focoY, 45;
    set $@focoRaio, 24;

    // Variaveis de Pre-inicio
    set $@ref, 1388; // 1388 são Yetis. Não deve ser usado...

    // Quantos jogadores estão desafiando o chefão? Se não tiver ninguém, conte mais 90 segundos, nada de trapaça.
    set $@inFront, getareausers("boss", $@focoX-$@focoRaio, $@focoY-$@focoRaio, $@focoX+$@focoRaio, $@focoY+$@focoRaio);
    if ($@inFront < 1) setnpctimer 0;
    if ($@inFront < 1) announce "Próxima luta na Arena Boss em 1,5 minutos contra BOSS", 0;
    if ($@inFront < 1) end;

    // Chega. Vamos começar.
    stopnpctimer;
    announce $@inFront+" jogadores desafiam " + $BOSS_name$ + " na Arena Boss!", 0;


    if ($BOSS_name$ == "yeti" || $BOSS_name$ == "rei") goto L_multi;

    // Qual o ID dos reforços?
    if ($BOSS_name$ == "VermeRei") set $@ref, 1006; // vermeGigante
    if ($BOSS_name$ == "foice")    set $@ref, 1371; // espectroSombrio 1347 → Trocou para limodeFogo 1371 por balanço.
    if ($BOSS_name$ == "Tarantula") set $@ref, 1349; // aranhaMarrom
    if ($BOSS_name$ == "sasquatch") set $@ref, 1307; // FadaTraidora
    if ($BOSS_name$ == "Nightmare") set $@ref, 1308; // dragaoVerde

    if ($BOSS_name$ == "VermeRei") set $@bb, 1401;
    if ($BOSS_name$ == "foice")    set $@bb, 1389; // TODO UNTESTED
    if ($BOSS_name$ == "Tarantula") set $@bb, 1336; // TODO UNTESTED
    if ($BOSS_name$ == "sasquatch") set $@bb, 1390; // TODO UNTESTED
    if ($BOSS_name$ == "Nightmare") set $@bb, 1400; // TODO UNTESTED


    // 1379 e 1378 → ID de monstro
    monster "boss", $@focoX, $@focoY, "Poderoso Chefão", $@bb, 1, "PoderosoChefao::OnBossDeath"; // Cria Chefão!!
    areamonster "boss", $@focoX-$@focoRaio, $@focoY-$@focoRaio, $@focoX+$@focoRaio, $@focoY+$@focoRaio, "", $@ref, ($@inFront*2), "PoderosoChefao::OnPetDeath"; //Reforços: Sumona dentro de toda arena! 2 para cada!
//    close2;
//    return;
    end;


L_multi:
    // Reforços (padrão: yeti)
    set $@ref, 1071; // Elemento de Gelo
    set $@ref2, 1085;// Cav. de Gelo

    // Se for desafio de reis, redefine reforços
    if ($BOSS_name$ == "rei") set $@ref2, 1378;
    if ($BOSS_name$ == "rei") set $@ref, 1383;

    // Cria chefões de acordo
    if ($BOSS_name$ == "yeti") areamonster "boss", $@focoX-4, $@focoY-4, $@focoX+4, $@focoY+4, "Yeti (Boss)", 1388, 4, "PoderosoChefao::OnBossDeath";
    if ($BOSS_name$ == "rei") monster "boss", $@focoX, $@focoY, "Rei Gélido (Boss)", 1380, 1, "PoderosoChefao::OnBossDeath"; // Cria Rei Gélido
    if ($BOSS_name$ == "rei") monster "boss", $@focoX, $@focoY, "Rei Stone (Boss)", 1385, 1, "PoderosoChefao::OnBossDeath"; // Cria Rei Stone

    announce "Lute contra: "+$BOSS_name$, 0;

    // Cria reforços "normalmente". São 2 de cada tipo por jogador, totalizando 4.
    areamonster "boss", $@focoX-$@focoRaio, $@focoY-$@focoRaio, $@focoX+$@focoRaio, $@focoY+$@focoRaio, "", $@ref, ($@inFront*2), "PoderosoChefao::OnPetDeath"; //Reforços: Sumona dentro de toda arena! 4 para cada! Ref1
    areamonster "boss", $@focoX-$@focoRaio, $@focoY-$@focoRaio, $@focoX+$@focoRaio, $@focoY+$@focoRaio, "", $@ref2, ($@inFront*2), "PoderosoChefao::OnPetDeath"; //Reforços: Sumona dentro de toda arena! 4 para cada! Ref2
    end;



// FIXME
OnBossDeath:
    set $@mobFront, mobcount("boss", "PoderosoChefao::OnBossDeath")+1;
    if ($@mobFront > 0) mapannounce "boss", "##1Aviso: Ainda existe "+$@mobFront+" monstro(s) tipo BOSS para matar!", 0;
    if ($@mobFront > 0) end;

    //mapannounce "boss", "##2O chefão morreu!", 0;
    //set $@inFront, getareausers("boss", $@focoX-$@focoRaio, $@focoY-$@focoRaio, $@focoX+$@focoRaio, $@focoY+$@focoRaio);
    //set $BOSS_limpo, 1; //← Volta ao incício
    killmonster "boss", "PoderosoChefao::OnPetDeath";
    //mapannounce "boss", "##4Real!", 0;

    announce "##3O chefe da Arena Boss foi derrotado!", 0;
    //announce "##3O chefe da Arena Boss foi derrotado pela equipe de "+$BOSS_lider$+"! Sobreviventes: "+$@inFront, 0;

    //misceffect 313; //← O que isto faz...
    end;


OnPetDeath:
    set $@inFront, getareausers("boss", 20, 20, 69, 69);
    set $@mobFront, mobcount("boss", "PoderosoChefao::OnPetDeath")+1;
    mapannounce "boss", "Monstros Restantes: "+$@mobFront, 0;
    if ($@inFront <= 3) mapannounce "boss", "Aviso: Apenas "+$@inFront+" jogador(es) vivo(s)!", 0;

    end;

}

boss,26,22,0|script|Guardião da Arena|141,0,0
{
    set $@mobitto, mobcount("boss", "PoderosoChefao::OnPetDeath")+1;
    set $@mobonno, mobcount("boss", "PoderosoChefao::OnBossDeath")+1;
    set $@mobFront, $@mobitto + $@mobonno;
    if ($@mobFront > 0) mes "Monstros Restantes: "+ $@mobFront;
    if ($@mobFront > 0) close;

    // Vamos aproveitar e garantir que está tudo certo... Uma proteção básica para grandes equivocos.
    if ($@mobonno <= 0 && ($BOSS_timer > gettimetick(2)+3600)) set $BOSS_limpo, 1;
	//mes "Existem "+$@mobonno+" chefões e "+$@mobitto+" monstros ativos no momento.";
	//mes "";

    mes "[Guardião da Arena]";
    mes "Deseja voltar?";
    menu
        "Sim.", L_ciao,
        "Não.", L_close;

L_close:
    close;

L_ciao:
    close2;
    warp "008-2", 210, 21;
	end;

}
